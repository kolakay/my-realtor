@extends('Front.Layouts.base')
@section('header')
   @include('Front.includes.header')
@endsection
@section('content')
    <!--Page Title-->
    <section class="page-title" style="background-image:url({{ asset('front-assets/images/background/bg-page-title-1.jpg')}});">
        <div class="auto-container">
            <div class="clearfix">
                <!--Title -->
                <div class="title-column">
                    <h1>Contact</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{ route('home.index') }}">Home</a></li>
                        <li class="active">Contact Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <!--Contact Section-->
    <section class="contact-section" id="app">
        <div class="contactloader" v-if="isLoading == true"></div>
        <div class="auto-container">
            <div class="row clearfix">

                <!--Contact Column-->
                <div class="form-column col-md-8 col-sm-12 col-xs-12">

                    <!--Contact Form Form-->
                    <div class="default-form contact-form">
                        <h2>SEND US MESSAGE</h2>
                        @csrf
                        {{-- <form method="post" action="sendemail.php" id="contact-form"> --}}
                            <div class="row clearfix">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="username" v-model="user.name" placeholder="Name">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email" v-model="user.email" placeholder="Email">
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="subject" v-model="user.subject" placeholder="Subject">
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <textarea name="message" placeholder="Your Message" v-model="user.message"></textarea>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-two" @click="submitRequest">Submit</button>
                                </div>
                            </div>
                        {{-- </form> --}}
                    </div>

                </div>

                <!--Contact Column-->
                <div class="contact-column col-md-4 col-sm-12 col-xs-12">

                    <!--Contact Info-->
                    <div class="contact-info">
                        <h2>ADDRESS</h2>
                        <ul>
                            <li class="address"><div class="icon"><span class="flaticon-map"></span></div> No 4 Unity Close, Ada George, Portharcourt Rivers State.</li>
                            <li><div class="icon"><span class="flaticon-technology"></span></div> +2348032813908</li>
                            <li><div class="icon"><span class="flaticon-note"></span></div> Email: info@eproagent.com</li>

                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </section>


    <!--Map Section-->
    <section class="map-section">
        <!--Map Box-->
        <div class="map-box">
            <!--Map Canvas-->
            <div class="map-canvas"
                data-zoom="8"
                data-lat="-37.817085"
                data-lng="144.955631"
                data-type="roadmap"
                data-hue="#ffc400"
                data-title="Envato"
                data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
            </div>
        </div>
    </section>

@endsection
@section('scripts')
<script src="http://maps.google.com/maps/api/js?key="></script>
<script src="{{ asset('front-assets/js/map-script.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
        <script src="{{ asset('js/axios.js') }}"></script>
        <script src="{{ asset('js/vue.js') }}"></script>
        <script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script>
    if (window.Vue) {
        const vueApp = new Vue({
            el: '#app',
            data : {
                user: {
                    name: '',
                    email: '',
                    subject:'',
                    message: '',

                },
                url : {
                    contact: `{{ route('home.contact.message') }}`
                },
                isLoading : false,

            },
            mounted(){
            },
            methods : {
                submitRequest(){
                    const formData = new FormData();

                    for ( var key in this.user ) {
                        let value = this.user[key];
                        formData.append(key, value);
                    }

                    this.isLoading = true;

                    formData.append('_token', $('input[name=_token]').val());

                    axios.post(this.url.contact, formData)
                        .then((response) => {
                            this.isLoading = false;
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                        })
                        .catch( (error) => {
                            this.isLoading = false;
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        });

                }
            }
        });
    }
</script>
@endsection

