@extends('Front.Layouts.base')
@section('header')
   @include('Front.includes.header')
@endsection
@section('content')
    <style>
        .post-thumb img{
            height: 79px !important;
            object-fit: cover;
        }
    </style>
    <!--Page Title-->
    <section class="page-title" style="background-image:url({{ asset('front-assets/images/background/bg-page-title-1.jpg')}});">
    	<div class="auto-container">
        	<div class="clearfix">
            	<!--Title -->
            	<div class="title-column">
                	<h1>PROPERTIES</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{ route('home.index') }}">Home</a></li>
                        <li class="active">Properties</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <!--Sidebar Page-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">

                    <!--Property Listing-->
                	<section class="property-listing">

                        <!--Layout Controls-->
                        <div class="layout-controls">
                            <div class="inner clearfix">
                                <div class="current-layout"><div class="icon"><span class="flaticon-menu-2"></span></div> <h5>PROPERTIES GRID</h5></div>
                                <div class="layout-options">
                                    <ul class="clearfix">
                                        <li class="control current"><a href="#"><span class="flaticon-menu-2"></span></a></li>
                                        <li class="control"><a href="#"><span class="flaticon-list-1"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <!--Default Property Box-->
                            @foreach($assets as $asset)
                            <div class="default-property-box col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <figure class="image">
                                            <a href="{{ route('home.properties.details', $asset->slug) }}">
                                                @if(isset($asset->assetImages[0]))
                                                    <img src="{{ $asset->assetImages[0]->src }}" class="img-responsive" alt="">
                                                @else
                                                    <img src="{{ asset('front-assets/images/resource/featured-image-1.jpg') }}" alt="">
                                                @endif
                                            </a>
                                        </figure>
                                        <div class="upper-info clearfix">
                                            <div class="property-label">{{ $asset->sale_type }}</div>
                                            <div class="add-fav"><a href="#"><span class="fa fa-heart-o"></span></a></div>
                                        </div>
                                        <div class="property-price">₦ {{ $asset->formatted_price }}</div>
                                    </div>
                                    <div class="lower-content">
                                        <div class="property-title">
                                            <h3><a href="{{ route('home.properties.details', $asset->slug) }}">{{ $asset->title }}</a></h3>
                                            <div class="location">{{ $asset->asset_location}} </div>
                                        </div>
                                        <div class="text-desc">{{ $asset->description }}</div>
                                        {{-- <div class="property-specs">
                                            <ul class="clearfix">
                                                <li><span class="icon flaticon-bed-1"></span> 5</li>
                                                <li><span class="icon flaticon-vintage-bathtub"></span> 3</li>
                                                <li><span class="icon flaticon-car-inside-a-garage"></span> 2</li>
                                                <li><span class="icon flaticon-blog-template"></span> 1240 sq ft</li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                            @endforeach


                        </div>

                        <!-- Styled Pagination -->
                        <div class="styled-pagination">
                            {{ $assets->appends(Request::get('asset'))->links()}}
                        </div>

                	</section>

                </div>
                <!--Content Side-->

                <!--Sidebar-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">

						<!-- Recent Posts -->
                        <div class="sidebar-widget prop-search-widget">
                        	<!--Search Form-->
                            <div class="search-form-panel">
                            	<div class="widget-header">Search for Properties</div>
                                <div class="form-box">
                                    <div class="default-form property-search-form">
                                        <form method="get" action="#">
                                            <div class="row clearfix">
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                	<div class="field-label">Keyword</div>
                                                    <input type="text" name="field-name" value="" placeholder="Enter Your Keyword" required >
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                	<div class="field-label">Property Type</div>
                                                    <select class="custom-select-box">
                                                        <option>Property Type</option>
                                                        <option>Residential</option>
                                                        <option>Commercial</option>
                                                        <option>Agriculture</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                	<div class="field-label">Status</div>
                                                    <select class="custom-select-box">
                                                        <option>Status</option>
                                                        <option>Available</option>
                                                        <option>Sold</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                	<div class="field-label">Location</div>
                                                    <input type="text" name="field-name" value="" placeholder="Enter Location" required >
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                	<div class="field-label">Bedroom</div>
                                                    <select class="custom-select-box">
                                                        <option>All</option>
                                                        <option>2</option>
                                                        <option>5</option>
                                                        <option>7</option>
                                                        <option>10+</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                	<div class="field-label">Bathroom</div>
                                                    <select class="custom-select-box">
                                                        <option>All</option>
                                                        <option>2</option>
                                                        <option>5</option>
                                                        <option>7</option>
                                                        <option>10+</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                    <div class="range-slider-one">
                                                        <div class="slider-header">
                                                            <div class="clearfix">
                                                                <div class="title">Price ($):</div>
                                                                <div class="input"><input type="text" class="property-amount" name="field-name" readonly></div>
                                                            </div>
                                                        </div>

                                                        <div class="price-range-slider"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                    <div class="range-slider-one">
                                                        <div class="slider-header">
                                                            <div class="clearfix">
                                                                <div class="title">Area (ft<sup>2</sup>):</div>
                                                                <div class="input"><input type="text" class="area-size" name="field-name" readonly></div>
                                                            </div>
                                                        </div>

                                                        <div class="area-range-slider"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                    <button type="submit" class="theme-btn btn-style-two">SEARCH Properties</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Recent Posts -->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title"><h3>Similar Properties</h3></div>
                            @foreach($featuredAssets as $featuredAsset)
                                <article class="post">
                                    <figure class="post-thumb">
                                        <a href="{{ route('home.properties.details', $featuredAsset->slug) }}">
                                            @if(isset($featuredAsset->assetImages[0]))
                                                <img src="{{ $featuredAsset->assetImages[0]->src }}" alt="">
                                            @else
                                                <img src="{{ asset('front-assets/images/resource/post-thumb-1.jpg') }}" alt="">
                                            @endif
                                        </a>
                                    </figure>
                                    <h4><a href="{{ route('home.properties.details', $featuredAsset->slug) }}">{{ $featuredAsset->title }}</a></h4>
                                    <div class="location"><span class="fa fa-map-marker"></span> {{ $featuredAsset->asset_location }}</div>
                                    <div class="price"><strong>₦ {{ $featuredAsset->formatted_price }}</strong></div>
                                </article>
                            @endforeach
                        </div>

                    </aside>


                </div>
                <!--Sidebar-->

            </div>
        </div>
    </div>

@endsection
