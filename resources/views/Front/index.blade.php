@extends('Front.Layouts.base')
@section('header')
   @include('Front.includes.header')
@endsection
@section('content')

    <!--Map Banner-->
    <section class="page-title" style="background-image:url({{ asset('front-assets/images/background/bg-page-title-1.jpg') }});">
    	<div class="auto-container">
        	<div class="clearfix">
            	<!--Title -->

            </div>
        </div>
    </section>

    <!--Default Search Section-->
    <section class="default-search-section">
    	<div class="auto-container">
        	<!--Search Form-->
            <div class="search-form-panel">
                <div class="form-box">
                    <div class="default-form property-search-form">
                        <form method="get" action="#">
                            <div class="row clearfix">
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <select class="custom-select-box">
                                        <option>Location</option>
                                        <option>UAE</option>
                                        <option>USA</option>
                                        <option>Australia</option>
                                        <option>Russia</option>
                                        <option>Pakistan</option>
                                        <option>China</option>
                                        <option>India</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <select class="custom-select-box">
                                        <option>Category</option>
                                        <option>Rental</option>
                                        <option>For Sale</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <select class="custom-select-box">
                                        <option>Property Type</option>
                                        <option>Residential</option>
                                        <option>Commercial</option>
                                        <option>Agriculture</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <select class="custom-select-box">
                                        <option>Status</option>
                                        <option>Available</option>
                                        <option>Sold</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <div class="range-slider-one">
                                        <div class="slider-header">
                                            <div class="clearfix">
                                                <div class="title">Price ($):</div>
                                                <div class="input"><input type="text" class="property-amount" name="field-name" readonly></div>
                                            </div>
                                        </div>

                                        <div class="price-range-slider"></div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <div class="range-slider-one">
                                        <div class="slider-header">
                                            <div class="clearfix">
                                                <div class="title">Area (ft<sup>2</sup>):</div>
                                                <div class="input"><input type="text" class="area-size" name="field-name" readonly></div>
                                            </div>
                                        </div>

                                        <div class="area-range-slider"></div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <select class="custom-select-box">
                                        <option>Rooms</option>
                                        <option>1-4</option>
                                        <option>5-8</option>
                                        <option>9-12</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-two">SEARCH</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Property Listing-->
    <section class="property-listing">
    	<div class="auto-container">
        	<!--Heading-->
            <div class="sec-title centered">
            	<div class="subtitle">Properties for sale and rent</div>
                <h2>OUR LATEST LISTINGS</h2>
            </div>

        	<div class="row clearfix">
                <!--Default Property Box-->
                @foreach($assets as $asset)
                <div class="default-property-box col-lg-4 col-md-6 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                            <figure class="image">
                                <a href="{{ route('home.properties.details', $asset->slug) }}">
                                    @if(isset($asset->assetImages[0]))
                                        <img src="{{ $asset->assetImages[0]->src }}" alt="">
                                    @else
                                        <img src="{{ asset('front-assets/images/resource/featured-image-1.jpg') }}" alt="">
                                    @endif
                                </a>
                            </figure>
                            <div class="upper-info clearfix">
                            	<div class="property-label">{{ $asset->sale_type }}</div>
                                <div class="add-fav"><a href="#"><span class="fa fa-heart-o"></span></a></div>
                            </div>
                            <div class="property-price">₦ {{ $asset->formatted_price }}</div>
                        </div>
                        <div class="lower-content">
                        	<div class="property-title">
                            	<h3><a href="{{ route('home.properties.details', $asset->slug) }}">{{ $asset->title }}</a></h3>
                                <div class="location">{{ $asset->asset_location}} </div>
                            </div>
                            <div class="text-desc">{{ $asset->description }}</div>
                            {{-- <div class="property-specs">
                            	<ul class="clearfix">
                                    <li><span class="icon flaticon-bed-1"></span> 5</li>
                                    <li><span class="icon flaticon-vintage-bathtub"></span> 3</li>
                                    <li><span class="icon flaticon-car-inside-a-garage"></span> 2</li>
                                    <li><span class="icon flaticon-blog-template"></span> 1240 sq ft</li>
                                </ul>
                            </div> --}}
                        </div>
                    </div>
                </div>
                @endforeach

            </div>

            <div class="view-all"><a href="{{ route('home.properties') }}" class="theme-btn btn-style-three">All Listing</a></div>

        </div>
    </section>


    <!--Default Section-->
    <section class="featured-properties-two">
    	<div class="auto-container">
        	<!--Heading-->
            <div class="sec-title">
            	<div class="subtitle">A selection of rental properties</div>
                <h2>FEATURED PROPERTIES</h2>
            </div>

            <div class="carousel-outer">
                <div class="featured-properties-carousel owl-theme owl-carousel">
                    <!--Slide Item-->
                    @foreach($featuredAssets as $featuredAsset)
                    <div class="slide-item">
                        <div class="slide-container clearfix">
                            <!--Image Column-->
                            @if(isset($featuredAsset->assetImages[0]))
                                <div class="image-column" style="background-image:url({{ $featuredAsset->assetImages[0]->src }});">
                            @else
                                <div class="image-column" style="background-image:url({{ asset('front-assets/images/resource/fluid-image-1.jpg') }});">
                            @endif
                                <figure class="image">
                                    @if(isset($featuredAsset->assetImages[0]))
                                        <img  src="{{ $featuredAsset->assetImages[0]->src }}" alt="">
                                    @else
                                        <img src="{{ asset('front-assets/images/resource/fluid-image-1.jpg') }}" alt="">
                                    @endif
                                </figure>
                                <a href="{{ route('home.properties.details', $featuredAsset->slug) }}" class="lightbox-image overlay-link"></a>
                            </div>

                            <!--Content Column-->
                            <div class="content-column">
                                <div class="inner">
                                    <!--Slide Header-->
                                    <div class="slide-header">
                                        <h3><a href="{{ route('home.properties.details', $featuredAsset->slug) }}">{{ $featuredAsset->title }}</a></h3>
                                        <div class="location">{{ $featuredAsset->asset_location }}</div>
                                        <div class="info clearfix">
                                            <div class="price">₦ {{ $featuredAsset->formatted_price }}</div>
                                            <div class="prop-label">{{ $featuredAsset->sale_type }}</div>
                                        </div>
                                    </div>
                                    <div class="description">{{ $featuredAsset->description }}</div>
                                    <!--Info-->
                                    <div class="agent-info clearfix">
                                        <div class="agent">
                                            <figure class="image img-circle"><img class="img-circle" src="{{ asset('front-assets/images/resource/author-thumb-1.jpg') }}" alt=""></figure><div class="name">Mark Smith</div></div>
                                        <div class="add-fav"><a href="#"><span class="fa fa-heart-o"></span></a></div>
                                    </div>
                                    <!--Specs-->
                                    {{-- <div class="property-specs">
                                        <ul class="clearfix">
                                            <li><span class="icon flaticon-bed-1"></span> 4 Bedromms</li>
                                            <li><span class="icon flaticon-vintage-bathtub"></span> 3 Bathrooms</li>
                                            <li><span class="icon flaticon-car-inside-a-garage"></span> 2 Garage</li>
                                            <li><span class="icon flaticon-blog-template"></span> 1210 sq ft</li>
                                        </ul>
                                    </div> --}}
                                </div>
                            </div>

                        </div>
                    </div>
                    @endforeach
                    <!--End Slide Item-->

                </div>
            </div>

        </div>
    </section>


    <!--Sponsors Section-->
    <section class="sponsors-section">
    	<!-- <div class="auto-container">

            <ul class="sponsors-carousel owl-theme owl-carousel">
            	<li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/1.png')}}" alt=""></a></figure></li>
                <li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/2.png')}}" alt=""></a></figure></li>
                <li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/3.png')}}" alt=""></a></figure></li>
                <li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/4.png')}}" alt=""></a></figure></li>
                <li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/5.png')}}" alt=""></a></figure></li>
                <li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/1.png')}}" alt=""></a></figure></li>
                <li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/2.png')}}" alt=""></a></figure></li>
                <li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/3.png')}}" alt=""></a></figure></li>
                <li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/4.png')}}" alt=""></a></figure></li>
                <li><figure class="image"><a href="#"><img src="{{ asset('front-assets/images/sponsors/5.png')}}" alt=""></a></figure></li>
            </ul>

        </div> -->
    </section>
@endsection
