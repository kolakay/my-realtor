<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- Stylesheets -->
        <link href="{{ asset('front-assets/css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('front-assets/css/style.css') }}" rel="stylesheet">
        <link rel="shortcut icon" href="{{ asset('front-assets/images/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('front-assets/images/favicon.ico') }}" type="image/x-icon">
        <!-- Responsive -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="robots" content="index, follow" />
        <link rel="canonical" href="https://www.eproagent.com" />
        <meta name="robots" content="index, follow" />

        @yield('header')
        <link href="{{ asset('front-assets/css/responsive.css') }}" rel="stylesheet">
        <link href="{{ asset('front-assets/css/custom.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
        <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
    </head>

    <body>

        <div class="page-wrapper">

            <!-- Preloader -->
            <div class="preloader"></div>

            <!-- Main Header / Header Style Two-->
            <header class="main-header header-style-two">
                <!-- Header Top-->
                <div class="header-top-one">
                    <div class="auto-container">
                        <div class="clearfix">

                            <!--Top Left-->
                            <div class="top-left">
                                <ul class="social-links">
                                    <li><a href="https://web.facebook.com/E-Pro-Agent-100705331473448/?view_public_for=100705331473448&_rdc=1&_rdr" target="_blank"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="https://twitter.com/eproagent?s=20" target="_blank"><span class="fa fa-twitter"></span></a></li>
                                    <!-- <li><a href="#"><span class="fa fa-google-plus"></span></a></li> -->
                                    <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                    <li><a href="#"><span class="fa fa-pinterest"></span></a></li>
                                </ul>
                            </div>

                            <!--Top Right-->
                            <div class="top-right top-links">
                                <ul class="clearfix">
                                    <li><a href="#"><span class="icon fa fa-phone"></span> +2348032813908</a></li>
                                    <li><a href="mailto:info@eproagent.com"><span class="icon fa fa-envelope-o"></span> info@eproagent.com</a></li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div><!-- Header Top End -->


                <!-- Header Lower -->
                <div class="header-lower">
                    <div class="main-box">
                        <div class="auto-container">
                            <div class="outer-container clearfix">
                                <!--Logo Box-->
                                <div class="logo-box">
                                    <div class="logo"><a href="index.html" title="E-pro Agent"><img src="{{ asset('front-assets/images/h-logo.svg')}}" alt="E-pro Agent" title="E-pro Agent"></a></div>
                                </div>

                                <!--Nav Outer-->
                                <div class="nav-outer clearfix">
                                    <!-- Main Menu -->
                                    <nav class="main-menu">
                                        <div class="navbar-header">
                                            <!-- Toggle Button -->
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div class="navbar-collapse collapse clearfix">
                                            <ul class="navigation clearfix">
                                                <li class="{{ ($page == 'index')? 'current': '' }}">
                                                    <a href="{{ route('home.index') }}">Home</a>
                                                </li>
                                                {{-- <li class="{{ ($page == 'about')? 'current': '' }}"><a href="{{ route('home.about') }}">About Us</a></li> --}}
                                                {{-- <li class="dropdown"><a href="#">Our Agents</a>
                                                    <ul>
                                                        <li><a href="agents.html">Our Agents</a></li>
                                                        <li><a href="agent-single.html">Single Agent</a></li>
                                                    </ul>
                                                </li> --}}
                                                <li class="{{ ($page == 'properties')? 'current': '' }}">
                                                    <a href="{{ route('home.properties') }}">Properties</a>
                                                </li>
                                                {{-- <li><a href="gallery.html">Gallery</a> </li>
                                                <li class="dropdown"><a href="#">Blog</a>
                                                    <ul>
                                                        <li><a href="blog.html">Blog</a></li>
                                                        <li><a href="blog-details.html">Blog Details</a></li>
                                                    </ul>
                                                </li> --}}
                                                <li class="{{ ($page == 'request')? 'current': '' }}"><a href="{{ route('home.request') }}">Make A Request</a></li>
                                                <li class="{{ ($page == 'contact')? 'current': '' }}"><a href="{{ route('home.contact') }}">Contact Us</a></li>

                                            </ul>
                                        </div>
                                    </nav><!-- Main Menu End-->

                                </div><!--Nav Outer End-->
                            </div>

                        </div>
                    </div>
                </div>

                <!--Sticky Header-->
                <div class="sticky-header">
                    <div class="auto-container clearfix">
                        <!--Logo-->
                        <div class="logo pull-left">
                            <a href="index.html" class="img-responsive" title="E-pro Agent">
                                <img src="{{ asset('front-assets/images/logo-small.png') }}" alt="E-pro Agent" title="E-pro Agent">
                            </a>
                        </div>

                        <!--Right Col-->
                        <div class="right-col pull-right">
                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <div class="navbar-header">
                                    <!-- Toggle Button -->
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        <li class="{{ ($page == 'index')? 'current': '' }}">
                                            <a href="{{ route('home.index') }}">Home</a>
                                        </li>
                                        {{-- <li class="{{ ($page == 'about')? 'current': '' }}">
                                            <a href="{{ route('home.about') }}">About Us</a>
                                        </li> --}}
                                        {{-- <li class="dropdown"><a href="#">Our Agents</a>
                                            <ul>
                                                <li><a href="agents.html">Our Agents</a></li>
                                                <li><a href="agent-single.html">Single Agent</a></li>
                                            </ul>
                                        </li> --}}
                                        <li class="{{ ($page == 'properties')? 'current': '' }}">
                                            <a href="{{ route('home.properties') }}">Properties</a>
                                        </li>
                                        {{-- <li><a href="gallery.html">Gallery</a> </li>
                                        <li class="dropdown"><a href="#">Blog</a>
                                            <ul>
                                                <li><a href="blog.html">Blog</a></li>
                                                <li><a href="blog-details.html">Blog Details</a></li>
                                            </ul>
                                        </li> --}}
                                        <li class="{{ ($page == 'contact')? 'current': '' }}"><a href="{{ route('home.contact') }}">Contact Us</a></li>

                                    </ul>
                                </div>
                            </nav><!-- Main Menu End-->
                        </div>

                    </div>
                </div><!--End Sticky Header-->

            </header>
            <!--End Main Header -->

            @yield('content')

            <!--Main Footer-->
            <footer class="main-footer">
                <!--Widgets Section-->
                <div class="widgets-section">
                    <div class="auto-container">
                        <div class="row clearfix">
                            <!--Big Column-->
                            <div class="big-column col-md-6 col-sm-12 col-xs-12">
                                <div class="row clearfix">
                                    <!--Footer Column-->
                                    <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                        <div class="footer-widget about-widget">
                                            <div class="logo"><a href="index.html"><img src="{{ asset('front-assets/images/f-logo.svg')}}" alt=""></a></div>
                                            <div class="widget-content">
                                                <div class="text">Science extraordinary claims require extra ordinary evidence from which we spring was preserve and cherish that pale blue dot take root and flourish! Hydrogen atoms.</div>
                                                <ul class="social-links">
                                                    <li><a href="https://web.facebook.com/E-Pro-Agent-100705331473448/?view_public_for=100705331473448&_rdc=1&_rdr" target="_blank"><span class="fa fa-facebook-f"></span></a></li>
                                                    <li><a href="https://twitter.com/eproagent?s=20" target="_blank"><span class="fa fa-twitter"></span></a></li>
                                                    <!-- <li><a href="#"><span class="fa fa-google-plus"></span></a></li> -->
                                                    <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                                    <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!--End Footer Column-->

                                    <!--Footer Column-->
                                    <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                        <div class="footer-widget contact-widget">
                                            <h2>ADDRESS</h2>
                                            <div class="widget-content">
                                                <ul class="contact-info">
                                                    <li><div class="icon"><span class="flaticon-location-pin"></span></div> No 4 Unity Close, Ada George, Portharcourt Rivers State.</li>
                                                    <li><div class="icon"><span class="flaticon-technology-4"></span></div>+2348032813908</li>
                                                    <li><div class="icon"><span class="flaticon-note"></span></div> info@eproagent.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!--End Footer Column-->
                                </div>
                            </div><!--End Big Column-->

                            <!--Big Column-->
                            <div class="big-column col-md-6 col-sm-12 col-xs-12">
                                <div class="row clearfix">


                                    <!--Footer Column-->
                                    <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                        <div class="footer-widget links-widget">
                                            <h2>LINKS</h2>
                                            <div class="widget-content">
                                                <ul class="list">
                                                    <li><a href="{{ route('home.index') }}">Home</a></li>
                                                    {{-- <li><a href="{{ route('home.about') }}">About</a></li> --}}
                                                    <li><a href="{{ route('home.properties') }}">Properties</a></li>
                                                    <li><a href="{{ route('home.contact') }}">Contact</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!--End Footer Column-->

                                    <!--Footer Column-->
                                    <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                        <div class="footer-widget subscribe-widget">
                                            <h2>NEWSLETTER</h2>
                                            <div class="widget-content">
                                                <div class="text">Subscribe to our newsletter and we will inform you about newset projects and promotions</div>
                                                <div class="newsletter-form">
                                                    <form method="post" action="contact.html">
                                                        <div class="form-group">
                                                            <input type="email" name="email" value="" placeholder="Enter Email" required>
                                                            <button type="submit" class="theme-btn"><span class="fa fa-paper-plane"></span></button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--End Footer Column-->

                                </div>
                            </div><!--End Big Column-->

                        </div>
                    </div>
                </div>

                <!--Footer Bottom-->
                <div class="footer-bottom">
                    <div class="auto-container clearfix">
                        <div class="copyright-text">&copy; {{ date('Y') }} E-pro Agent. All Rights Reserved</div>
                        <div class="footer-nav clearfix">
                            <ul class="clearfix">
                                <li><a href="{{ route('home.index') }}">Home</a></li>
                                {{-- <li><a href="{{ route('home.about') }}">About</a></li> --}}
                                <li><a href="{{ route('home.properties') }}">Properties</a></li>
                                <li><a href="{{ route('home.contact') }}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </footer>

        </div>
        <!--End pagewrapper-->

        <!--Scroll to top-->
        <div class="scroll-to-top scroll-to-target" data-target="html">
            <span class="fa fa-long-arrow-up"></span>
        </div>


        <script src="{{ asset('front-assets/js/jquery.js') }}"></script>
        <script src="{{ asset('front-assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('front-assets/js/jquery-ui.js') }}"></script>
        <script src="{{ asset('front-assets/js/jquery.fancybox.pack.js') }}"></script>
        <script src="{{ asset('front-assets/js/jquery.fancybox-media.js') }}"></script>
        <script src="{{ asset('front-assets/js/owl.js') }}"></script>
        <script src="{{ asset('front-assets/js/validate.js')}}"></script>
        <script src="{{ asset('front-assets/js/wow.js') }}"></script>
        <script src="{{ asset('front-assets/js/appear.js') }}"></script>
        <script src="{{ asset('front-assets/js/script.js') }}"></script>
        <script src="{{ asset('front-assets/js/banner-map.js') }}"></script>
        {{-- <script src="http://maps.google.com/maps/api/js?key=key&sensor=false&callback=initialize"></script> --}}

        @yield('scripts')
    </body>
</html>

