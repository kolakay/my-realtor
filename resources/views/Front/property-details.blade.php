@extends('Front.Layouts.base')
@section('header')
   @include('Front.includes.property-header')
@endsection
@section('content')
    <style>
        .figure-img img{
            height: 500px !important;
            object-fit: cover;
        }
        .post-thumb img{
            height: 79px !important;
            object-fit: cover;
        }
        .thumb-box img{
            height: 90px !important;
            object-fit: cover;
        }
    </style>
    <!--Page Title-->
    <section class="page-title" style="background-image:url({{ asset('front-assets/images/background/bg-page-title-1.jpg')}});">
    	<div class="auto-container">
        	<div class="clearfix">
            	<!--Title -->
            	<div class="title-column">
                	<h1>Property Details</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{ route('home.index') }}">Home</a></li>
                        <li class="active">Property Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <!--Sidebar Page-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Property DEtails-->
                    <section class="property-details">
                    	<div class="prop-header">
                        	<h3>{{ $asset->title }}</h3>
                            <div class="info clearfix">
                            	<div class="location">{{ $asset->asset_location }}</div>
                                <div class="prop-label">{{ $asset->sale_type }}</div>
                            </div>
                        </div>

                        <!--Product Carousel-->
                        <div class="product-carousel-outer">
                        	<div class="property-price">₦ {{ $asset->formatted_price }}</div>

                            <!--Product image Carousel-->
                            <ul class="prod-image-carousel owl-theme owl-carousel">
                                @if(isset($asset->assetImages[0]))
                                    @foreach($asset->assetImages as $image)
                                        <li>
                                            <figure class="image figure-img">
                                                <a class="lightbox-image option-btn" data-fancybox-group="example-gallery" href="{{ $image->src }}" title="{{ $asset->title }}">
                                                <img src="{{ $image->src }}" alt=""></a>
                                            </figure>
                                        </li>
                                    @endforeach
                                @else
                                    <li>
                                        <figure class="image figure-img">
                                            <a class="lightbox-image option-btn" data-fancybox-group="example-gallery" href="{{ asset('front-assets/images/resource/featured-image-1.jpg') }}" title="No Image">
                                            <img src="{{ asset('front-assets/images/resource/featured-image-1.jpg') }}" alt=""></a>
                                        </figure>
                                    </li>
                                @endif
                            </ul>

                            <!--Product Thumbs Carousel-->
                            <div class="prod-thumbs-carousel owl-theme owl-carousel">
                                @if(isset($asset->assetImages[0]))
                                    @foreach($asset->assetImages as $image)
                                        <div class="thumb-item">
                                            <figure class="thumb-box">
                                                <img src="{{ $image->src }}" alt="">
                                            </figure>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="thumb-item">
                                        <figure class="thumb-box">
                                            <img src="{{ asset('front-assets/images/resource/post-thumb-1.jpg') }}" alt="">
                                        </figure>
                                    </div>
                                @endif
                            </div>

                        </div><!--End Product Carousel-->

                        <div class="detail-content">
                        	<div class="text-content">
                            	<h3>PROPERTY DESCRIPTION</h3>
                                <p><strong>{{ $asset->description }}</strong></p>
                            </div>

                            {{-- <div class="property-specs">
                            	<div class="row clearfix">
                                	<div class="col-md-5 col-sm-5 col-xs-12">
                                    	<h3>PROPERTY INFORMATION</h3>
                                        <ul class="specs-list">
                                        	<li><div class="icon"><span class="flaticon-bed-1"></span></div> 3 Bedrooms</li>
                                            <li><div class="icon"><span class="flaticon-vintage-bathtub"></span></div> 2 Bathrooms</li>
                                            <li><div class="icon"><span class="flaticon-car-inside-a-garage"></span></div> 1 Garage</li>
                                            <li><div class="icon"><span class="flaticon-blog-template"></span></div> 1040 sq ft</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-7 col-sm-5 col-xs-12">
                                    	<h3>AMENITIES</h3>
                                        <div class="row clearfix">
                                			<div class="col-md-6 col-sm-6 col-xs-12">
                                                <ul class="list">
                                                    <li>Air Conditioning</li>
                                                    <li>Parking</li>
                                                    <li>Childrens Play</li>
                                                    <li>Gym</li>
                                                    <li>Jog Path</li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <ul class="list">
                                                    <li>Balcony</li>
                                                    <li>Badminton Court</li>
                                                    <li>Wifi</li>
                                                    <li>Lift</li>
                                                    <li>Swimming Pool</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}



                            <h3>CONTACT AGENT</h3>
                            <!--Contact Agent-->
                            <div class="contact-agent">
                                <section class="details">
                                    <div class="row clearfix">
                                        <!--Details Column-->
                                        <div class="details-column col-md-7 col-sm-6 col-xs-12">
                                            <div class="row clearfix">
                                                <!--Image Column-->
                                                <div class="image-column col-md-6 col-sm-4 col-xs-12">
                                                    <figure class="image"><img src="{{ asset('front-assets/images/resource/team-image-7.jpg')}}" alt=""></figure>
                                                </div>
                                                <!--Content Column-->
                                                <div class="content-column col-md-6 col-sm-8 col-xs-12">
                                                    <div class="inner">
                                                        <div class="upper-info">
                                                            <h3>Eva Rodriges</h3>
                                                            <div class="designation">Real Estate Agent</div>
                                                        </div>
                                                        <ul class="contact-info">
                                                            <li><div class="icon"><span class="fa fa-envelope"></span></div> eva@companyname.com</li>
                                                            <li><div class="icon"><span class="fa fa-phone"></span></div> 147 025 3689</li>
                                                            <li><div class="icon"><span class="fa fa-globe"></span></div> www.companyname.com</li>
                                                        </ul>
                                                        <ul class="social-links">
                                                            <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                                            <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                                            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--Form Column-->
                                        <div class="form-column col-md-5 col-sm-6 col-xs-12">
                                            <div class="inner">
                                                <div class="default-form agent-contact-form">
                                                    <form method="get" action="#">
                                                        <div class="clearfix">
                                                            <div class="form-group">
                                                                <input type="text" name="field-name" value="" placeholder="Name" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="email" name="field-name" value="" placeholder="Email" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea name="field-name" placeholder="Message" required></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit" class="theme-btn btn-style-two">SEND MESSAGE</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>

                    </section>
                </div>
                <!--Content Side-->

                <!--Sidebar-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">

						<!-- Recent Posts -->
                        <div class="sidebar-widget prop-search-widget">
                        	<!--Search Form-->
                            <div class="search-form-panel">
                            	<div class="widget-header">Search for Properties</div>
                                <div class="form-box">
                                    <div class="default-form property-search-form">
                                        <form method="get" action="#">
                                            <div class="row clearfix">
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                	<div class="field-label">Keyword</div>
                                                    <input type="text" name="field-name" value="" placeholder="Enter Your Keyword" required >
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                	<div class="field-label">Property Type</div>
                                                    <select class="custom-select-box">
                                                        <option>Property Type</option>
                                                        <option>Residential</option>
                                                        <option>Commercial</option>
                                                        <option>Agriculture</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                	<div class="field-label">Status</div>
                                                    <select class="custom-select-box">
                                                        <option>Status</option>
                                                        <option>Available</option>
                                                        <option>Sold</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                	<div class="field-label">Location</div>
                                                    <input type="text" name="field-name" value="" placeholder="Enter Location" required >
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                	<div class="field-label">Bedroom</div>
                                                    <select class="custom-select-box">
                                                        <option>All</option>
                                                        <option>2</option>
                                                        <option>5</option>
                                                        <option>7</option>
                                                        <option>10+</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                	<div class="field-label">Bathroom</div>
                                                    <select class="custom-select-box">
                                                        <option>All</option>
                                                        <option>2</option>
                                                        <option>5</option>
                                                        <option>7</option>
                                                        <option>10+</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                    <div class="range-slider-one">
                                                        <div class="slider-header">
                                                            <div class="clearfix">
                                                                <div class="title">Price ($):</div>
                                                                <div class="input"><input type="text" class="property-amount" name="field-name" readonly></div>
                                                            </div>
                                                        </div>

                                                        <div class="price-range-slider"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                    <div class="range-slider-one">
                                                        <div class="slider-header">
                                                            <div class="clearfix">
                                                                <div class="title">Area (ft<sup>2</sup>):</div>
                                                                <div class="input"><input type="text" class="area-size" name="field-name" readonly></div>
                                                            </div>
                                                        </div>

                                                        <div class="area-range-slider"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                    <button type="submit" class="theme-btn btn-style-two">SEARCH Properties</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Recent Posts -->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title"><h3>Similar Properties</h3></div>

                            @foreach($featuredAssets as $featuredAsset)
                                <article class="post">
                                    <figure class="post-thumb">
                                        <a href="{{ route('home.properties.details', $featuredAsset->slug) }}">
                                            @if(isset($featuredAsset->assetImages[0]))
                                                <img src="{{ $featuredAsset->assetImages[0]->src }}" alt="">
                                            @else
                                                <img src="{{ asset('front-assets/images/resource/post-thumb-1.jpg') }}" alt="">
                                            @endif
                                        </a>
                                    </figure>
                                    <h4><a href="{{ route('home.properties.details', $featuredAsset->slug) }}">{{ $featuredAsset->title }}</a></h4>
                                    <div class="location"><span class="fa fa-map-marker"></span> {{ $featuredAsset->asset_location }}</div>
                                    <div class="price"><strong>₦ {{ $featuredAsset->formatted_price }}</strong></div>
                                </article>
                            @endforeach

                        </div>

                    </aside>


                </div>
                <!--Sidebar-->

            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="http://maps.google.com/maps/api/js?key="></script>
<script src="{{ asset('front-assets/js/map-script.js') }}"></script>
@endsection
