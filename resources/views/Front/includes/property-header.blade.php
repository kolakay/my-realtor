<title>{{ $asset->title }} - E-Pro Agent</title>
<meta name="description" content="{{ $asset->description }}" />
<meta property="og:title" content="{{ $asset->title }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ route('home.properties.details', $asset->slug) }}" />
<meta property="og:image"       content="{{ $asset->assetImages[0]->src }}" />
<meta property="og:description" content="{{ $asset->description }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="twitter:title" content="{{ $asset->title }}">
<meta name="twitter:description" content="{{ $asset->description }}">
