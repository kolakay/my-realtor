<title>Properties for Sale and Rent in Portharcourt - E-Pro Agent</title>
<meta name="description" content="Looking for a property for sale/rent or lease in Portharcourt? We provide property seekers an easy way to find details of property in Portharcourt." />
<meta property="og:title" content="Real Estate & Property in Portharcourt for Sale and Rent - E-Pro Agent" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.eproagent.com" />
<meta property="og:image"       content="{{ (isset($seoAsset->assetImages[0]))?$seoAsset->assetImages[0]->src : ''}}" />
<meta property="og:description" content="Looking for a property for sale/rent or lease in Portharcourt? We provide property seekers an easy way to find details of property in Portharcourt.">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="twitter:title" content="Real Estate & Property in Portharcourt for Sale, Lease, Book and Rent - E-Pro Agent">
<meta name="twitter:description" content="Looking for a property for sale/rent or lease in Portharcourt? We provide property seekers an easy way to find details of property in Portharcourt.">
