@extends('Admin.Layouts.master')
@section('page-styles')
<link rel="stylesheet" href="{{ asset('admin-assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
{{-- <link href="{{ asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"> --}}
<style>
    #url-container{
        padding-left: 0px;
        padding-right: 30px;
    }
    .apartment-details, .type-container{
        margin-top: 30px;
    }
    .apartment-edit-container{
        max-height: 700px;
        min-height: 500px;
        overflow-y: scroll;
    }
    .apartment-edit-container::-webkit-scrollbar {
        width: 7px;
    }

    /* Track */
    .apartment-edit-container::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
        border-radius: 10px;
    }

    /* Handle */
    .apartment-edit-container::-webkit-scrollbar-thumb {
        background: #727cf5;
        border-radius: 3px;
    }

    /* Handle on hover */
    .apartment-edit-container::-webkit-scrollbar-thumb:hover {
        background: #727cf5;
    }

    .image-cancel{
        display: none;
        position: absolute;
        height: 100px;
        width: 100px;
        background: #FFF;
        text-align: right;
        /* padding-right: 5px; */
        opacity: .8;
        /* cursor: pointer; */
    }

    .preview-image:hover > .image-cancel {
        display: block;
    }


</style>
@endsection
@section('content')
    <div class="page-content" id="app">
        <div class="loader d-flex justify-content-center" v-if="isLoading == true">
            <div class="spinner-border loader__figure" role="status">
                <span class="sr-only">Loading...</span>

            </div>

        </div>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0">Edit Apartment</h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">

                {{-- <button type="button" class="btn btn-outline-info btn-icon-text mr-2 d-none d-md-block">
                    <i class="btn-icon-prepend" data-feather="download"></i>Import
                </button> --}}
                {{-- <a href="{{ route('user.checkout', $apartment->url) }}" target="_blank" class="btn btn-outline-primary btn-icon-text mr-2 mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="eye"></i> Preview
                </a> --}}
                <button type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0" v-on:click="storeApartment">
                    <i class="btn-icon-prepend" data-feather="download-cloud"></i> Save
                </button>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row position-relative">
                    <div class="col-12 chat-aside">
                        <div class="aside-content">
                            <div class="aside-body">
                                <ul class="nav nav-tabs mt-3" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">
                                            <div class="d-flex flex-row flex-lg-column flex-xl-row align-items-center">
                                            <i data-feather="home" class="icon-sm mr-sm-2 mr-lg-0 mr-xl-2 mb-md-1 mb-xl-0"></i>
                                            <p class="d-none d-sm-block">Apartment Details</p>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab" aria-controls="images" aria-selected="true">
                                            <div class="d-flex flex-row flex-lg-column flex-xl-row align-items-center">
                                            <i data-feather="instagram" class="icon-sm mr-sm-2 mr-lg-0 mr-xl-2 mb-md-1 mb-xl-0"></i>
                                            <p class="d-none d-sm-block">Apartment Images</p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <div class="apartment-edit-container">
                                    <div class="tab-content mt-3">
                                        @csrf
                                        @include('Admin.Includes.Apartments.details')
                                        @include('Admin.Includes.Apartments.images')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <textarea style="display:none" name="" id="apartmentId">{!! json_encode($apartment) !!}</textarea>
        <textarea style="display:none" name="" id="assetId">{!! json_encode($apartment->asset) !!}</textarea>
        <textarea style="display:none" name="" id="imagesId">{!! json_encode($apartment->images) !!}</textarea>
        <textarea style="display:none" name="" id="lgasId">{!! json_encode($lgas) !!}</textarea>
        <textarea style="display:none" name="" id="localityId">{!! json_encode($localities) !!}</textarea>

    </div>
@endsection
@section('page-scripts')
<script>
    if (window.Vue) {
        const vueApp = new Vue({
            el: '#app',
            data : {
                isLoading: false,
                details: {
                    asset : {
                        title: '',
                        sale_type: '',
                        category:'apartments',
                        address: '',
                        description: '',
                        price: '',
                        status:'in_active',
                        lga_id: 0,
                        locality_id: 0,
                    },
                    apartment: {
                        asset_id: '',
                        type: '',
                        total_rooms: 0,
                        bedrooms: '',
                        bathrooms: '',
                        square_meters: '',
                        parking_space:'',
                        furnishing:'',
                        features:[],

                    }
                },
                url : {
                    edit : `{{ route('admin.assets.apartments.edit') }}` +'/',
                    delete: `{{ route('admin.assets.apartments.delete') }}`+ '/',
                    create: `{{ route('admin.assets.apartments.add') }}`,
                    update: `{{ route('admin.assets.apartments.update') }}`,
                    images: `{{ route('admin.assets.apartments.images.upload') }}`,
                    deleteImage: `{{ route('admin.assets.apartments.image.delete') }}`,
                    locality: `{{ route('admin.assets.localities') }}`,
                },
                formErrors : [],
                images: [],
                lgas: [],
                localities: [{
                    id: "",
                    locality: ""
                }]


            },
            mounted(){
                this.details.apartment = JSON.parse($("#apartmentId").val());
                this.details.asset = JSON.parse($("#assetId").val());
                this.images = JSON.parse($("#imagesId").val());
                this.lgas = JSON.parse($("#lgasId").val());
                this.localities = JSON.parse($("#localityId").val());
            },
            computed: {
                // getApartments() {

                //     var result = this.apartments.filter((apartment) => {
                //         return apartment.name.toLowerCase().includes(this.filter.toLowerCase());
                //     });

                //     return result;
                // }
            },
            methods : {
                createAsset(){
                    // console.log(this.apartment)
                    const formData = new FormData();

                    for ( var key in this.apartment ) {
                        let value = this.apartment[key];
                        formData.append(key, value);
                    }

                    formData.append('_token', $('input[name=_token]').val());
                    this.isLoading = true;

                    axios.post(this.url.create, formData)
                        .then((response) => {
                            // console.log(response);
                            window.location = response.data.url;
                        })
                        .catch( (error) => {

                            if(error.response.data.errors){
                                const values = Object.values(error.response.data.errors);
                                for (let index = 0; index < values.length; index++) {
                                    for (let j = 0; j < values[index].length; j++) {
                                        const element = values[index][j];
                                        this.formErrors.push(element);
                                    }

                                }
                            }

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                            this.isLoading = false;
                        });
                },
                storeApartment(){
                    const formData = new FormData();

                    for ( var key in this.details ) {
                        let value = this.details[key];
                        value = JSON.stringify(value);
                        formData.append(key, value);
                    }

                    // console.log(this.details);
                    formData.append('_token', $('input[name=_token]').val());
                    this.isLoading = true;

                    axios.post(this.url.update, formData)
                        .then((response) => {
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                            this.isLoading = false;
                        })
                        .catch( (error) => {

                            if(error.response.data.errors){
                                const values = Object.values(error.response.data.errors);
                                for (let index = 0; index < values.length; index++) {
                                    for (let j = 0; j < values[index].length; j++) {
                                        const element = values[index][j];
                                        this.formErrors.push(element);
                                    }

                                }
                            }

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                            this.isLoading = false;
                        });
                },
                readImage() {
                    let formData = new FormData();
                    if (window.File && window.FileList && window.FileReader) {
                        var files = event.target.files; //FileList object

                        for (let i = 0; i < files.length; i++) {
                            var file = files[i];
                            if (!file.type.match('image')) continue;

                            formData.append('files[' + i + ']', file);
                            // this.images.push({
                            //     id:0,
                            //     src:  window.URL.createObjectURL(file)
                            // });
                        }

                        this.storeImages(formData);

                        //$("#pro-image").val('');
                    } else {
                        this.$notify.error({
                            title: 'Error',
                            message: 'Unable to complete request. Please update your browser'
                        });
                    }
                },
                storeImages(formData){
                    formData.append('_token', $('input[name=_token]').val());
                    formData.append('apartment_id', this.details.apartment.id);
                    this.isLoading = true;

                    axios.post(this.url.images, formData,{
                            headers: {
                                'Content-Type': 'multipart/form-data'
                            },
                        })
                        .then((response) => {
                            // console.log(response);
                            this.images = response.data.images;
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                            this.isLoading = false;
                        })
                        .catch( (error) => {

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });

                            this.isLoading = false;
                        });
                },
                deleteImage(id){
                    let formData = new FormData();
                    formData.append('_token', $('input[name=_token]').val());
                    formData.append('image_id', id);
                    formData.append('apartment_id', this.details.apartment.id);
                    this.isLoading = true;

                    axios.post(this.url.deleteImage, formData)
                        .then((response) => {
                            this.images = response.data.images;
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                            this.isLoading = false;
                        })
                        .catch( (error) => {

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                            this.isLoading = false;
                        });
                },
                getLocality(event){
                    let lgaId = event.target.value;
                    const formData = new FormData();
                    formData.append('_token', $('input[name=_token]').val());
                    formData.append('id', lgaId);

                    axios.post(this.url.locality, formData)
                        .then((response) => {

                            this.localities = response.data.localities;
                        })
                        .catch( (error) => {

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        });
                }
            }
        });
    }
</script>
@endsection
