@extends('Admin.Layouts.master')
@section('content')
    <div class="page-content" id="app">
        <div class="row flex-grow">
            <a data-toggle="modal" data-target="#defaultModal" class="card col-md-3 grid-margin stretch-card new-product">
                <div class="card-body text-center product-add">
                    <p style="padding:0px; margin:0px;">+ Asset</p>
                </div>
            </a>
            {{-- @foreach ($products as $product) --}}
                <div class="col-md-3 grid-margin stretch-card" v-for="product in products">
                    <div class="card">
                        <img src="{{ asset('admin-assets/images/placeholder.jpg') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                      </div>
                </div>
            {{-- @endforeach --}}

        </div>

    </div>


@endsection
@section('page-styles')
<style>
    .new-product{
        text-decoration: none;
        cursor:pointer;
        display: flex;
        align-items: center;
    }

    .new-product, .card{
        -webkit-box-shadow:  0 1px 2px 0 rgba(86,94,102,.15);
        -moz-box-shadow:  0 1px 2px 0 rgba(86,94,102,.15);
        box-shadow:  0 1px 2px 0 rgba(86,94,102,.15);
    }
    .new-product:hover, .new-product:active{
        text-decoration: none;
    }

    .new-product:hover, .card:hover{
        -webkit-box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        -moz-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
    }
    .c

    .form-float{
        margin-top:20px;
    }
    .form-select{
        margin-top:15px;
    }
    .products-container{
        display: flex;
    }
    .products-container .card{
        margin-bottom:0px;
    }
    .p-title{
        margin: 0px !important;
    }
    .p-price{
        margin-top:50px;
    }
    .product-footer{
        padding:20px;
    }
    .product-add{
        width: 100%;
    }
</style>
@endsection
