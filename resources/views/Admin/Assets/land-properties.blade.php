@extends('Admin.Layouts.master')
@section('content')
    <div class="page-content" id="app">
        <div class="loader d-flex justify-content-center" v-if="isLoading == true">
            <div class="spinner-border loader__figure" role="status">
                <span class="sr-only">Loading...</span>

            </div>

        </div>
        <div class="row flex-grow">
            <a href="#" data-toggle="modal" data-target="#addland" class="card col-md-3 grid-margin stretch-card new-land">
                <div class="card-body text-center land-add">
                    + Land Property
                </div>
            </a>
                <div class="col-md-3 grid-margin stretch-card" v-for="land in lands">
                    <div class="card">
                        <img :src="land.asset.first_image" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">@{{ land.asset.title }}</h5>
                            <p class="card-text">@{{ land.asset.description }}</p>
                            <p class="card-text"><small class="text-muted">Last updated @{{ land.updated_at }}</small></p>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex align-items-center mr-n1 icons">
                                <a :href="url.edit + land.id" class="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 icon-lg mr-3"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                </a>
                                {{-- <a :href="url.preview + land.url" target="_blank" class="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye icon-lg mr-0 mr-sm-3"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                </a> --}}
                                <a href="#" class="icon del-land" @click="deleteAsset(land)" data-placement="bottom" data-toggle="tooltip" title="Delete land">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash icon-lg mr-0 mr-sm-3"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                </a>

                                <a href="#" class="icon" @click="copyAsset(land.asset)" title="Copy Product Link">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard icon-lg mr-0 mr-sm-3"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>
                                </a>


                            </div>
                        </div>
                      </div>
                </div>

        </div>
        <div class="modal fade" id="addland" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h5 class="modal-title h4" id="myExtraLargeModalLabel">Add Asset</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row clearfix">
                            @csrf
                            <div class="col-md-12 alert-danger p-3" v-if="formErrors.length > 0">
                                <ul>
                                    <li v-for="formError in  formErrors">@{{ formError }}</li>

                                </ul>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Asset Title</label>
                                        <input type="text" v-model="land.title" name="asset_title"class="form-control" placeholder="Asset Title">

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Asset Price</label>
                                        <input type="text" v-model="land.price" name="asset_price"class="form-control" placeholder="Asset Price">

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Sale Type</label>
                                        <select class="form-control" v-model="land.sale_type">
                                            <option value="sale">Sale</option>
                                            <option value="rent">Rent</option>
                                            <option value="book">Book</option>
                                            <option value="lease">Lease</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">

                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Property Type</label>
                                        <select class="form-control" v-model="land.type">
                                            <option value="commercial">Commercial</option>
                                            <option value="industrial">Industrial</option>
                                            <option value="mix_used">Mix Used</option>
                                            <option value="residential">Residential</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Square Meters</label>
                                        <input type="text" v-model="land.square_meters" name="square_meters"class="form-control" placeholder="Square Meters">

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Plots</label>
                                        <input type="text" v-model="land.plots" name="plots"class="form-control" placeholder="Plots">

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Asset Status</label>
                                        <select class="form-control" v-model="land.status">
                                            <option value="active">Active</option>
                                            <option value="in_active">In-Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Asset LGA</label>
                                        <select class="form-control" v-model="land.lga_id" @change="getLocality($event)">
                                            <option v-for="lga in lgas" :value="lga.id">@{{ lga.lga }}</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Asset Locality</label>
                                        <select class="form-control" v-model="land.locality_id">
                                            <option v-for="locality in localities" :value="locality.id">@{{ locality.locality }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                            <label class="form-label">land Address</label>
                                        <textarea type="text" v-model="land.address" rows="5" name="land_address"class="form-control"></textarea>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                            <label class="form-label">land Description</label>
                                        <textarea type="text" v-model="land.description" rows="5" name="land_description"class="form-control"></textarea>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn waves-effect btn-primary" @click="createAsset">Create</button>
                            <button type="button" class="btn waves-effect btn-danger" data-dismiss="modal">Close</button>
                        </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" name="" id="landsId">{!! json_encode($lands) !!}</textarea>
        <textarea style="display:none" name="" id="lgasId">{!! json_encode($lgas) !!}</textarea>
    </div>


@endsection
@section('page-styles')
<style>
    .new-land{
        text-decoration: none;
        cursor:pointer;
        display: flex;
        align-items: center;
    }

    .new-land, .card{
        -webkit-box-shadow:  0 1px 2px 0 rgba(86,94,102,.15);
        -moz-box-shadow:  0 1px 2px 0 rgba(86,94,102,.15);
        box-shadow:  0 1px 2px 0 rgba(86,94,102,.15);
    }
    .new-land:hover, .new-land:active{
        text-decoration: none;
    }

    .new-land:hover, .card:hover{
        -webkit-box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        -moz-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
    }

    .form-float{
        margin-top:20px;
    }
    .form-select{
        margin-top:15px;
    }
    .land-add{
        display: grid;
        align-content: center;
    }
</style>
@endsection
@section('page-scripts')
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script src="{{ asset('assets/js/axios.js') }}"></script>
    <script src="{{ asset('assets/js/vue.js') }}"></script>
    <script src="https://unpkg.com/element-ui/lib/index.js"></script> --}}
    <script>
        if (window.Vue) {
            const vueApp = new Vue({
                el: '#app',
                data : {
                    isLoading: false,
                    lands : [],
                    land: {
                        title: '',
                        sale_type: '',
                        category:'lands',
                        address: '',
                        description: '',
                        price: '',
                        status:'in_active',
                        type:'',
                        square_meters:0,
                        plots:0

                    },
                    url : {
                        edit : `{{ route('admin.assets.lands.edit') }}` +'/',
                        delete: `{{ route('admin.assets.lands.delete') }}`+ '/',
                        create: `{{ route('admin.assets.lands.add') }}`,
                        locality: `{{ route('admin.assets.localities') }}`,
                        display: `{{ route('home.properties.details') }}`+ '/'
                    },
                    formErrors : [],
                    lgas: [],
                    localities: [{
                        id: "",
                        locality: ""
                    }],
                    defaultImage: `{{ asset('admin-assets/images/placeholder.jpg') }}`

                },
                mounted(){
                    this.lands = JSON.parse($("#landsId").val());
                    this.lgas = JSON.parse($("#lgasId").val());
                },
                computed: {
                    // getlands() {

                    //     var result = this.lands.filter((land) => {
                    //         return land.name.toLowerCase().includes(this.filter.toLowerCase());
                    //     });

                    //     return result;
                    // }
                },
                methods : {
                    createAsset(){
                        // console.log(this.land)
                        const formData = new FormData();

                        for ( var key in this.land ) {
                            let value = this.land[key];
                            formData.append(key, value);
                        }

                        formData.append('_token', $('input[name=_token]').val());
                        this.isLoading = true;

                        axios.post(this.url.create, formData)
                            .then((response) => {
                                // console.log(response);
                                window.location = response.data.url;
                            })
                            .catch( (error) => {

                                if(error.response.data.errors){
                                    const values = Object.values(error.response.data.errors);
                                    for (let index = 0; index < values.length; index++) {
                                        for (let j = 0; j < values[index].length; j++) {
                                            const element = values[index][j];
                                            this.formErrors.push(element);
                                        }

                                    }
                                }

                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                                this.isLoading = false;
                            });
                    },

                    deleteAsset(land){
                        const customAlert = swal({
                            title: 'Warning',
                            text: `Are you sure you want to delete this property? All features related to this property will be deleted. This action cannot be undone.`,
                            icon: 'warning',
                            closeOnClickOutside: false,
                            buttons: {
                                cancel: {
                                    text: "cancel",
                                    visible: true,
                                    className: "",
                                    closeModal: true,
                                },
                                confirm: {
                                    text: "Confirm",
                                    value: 'delete',
                                    visible: true,
                                    className: "btn-danger",
                                }
                            }
                        });

                        customAlert.then(value => {
                            if (value == 'delete') {
                                window.location = this.url.delete + land.asset_id;
                            }
                        })
                    },
                    getLocality(event){
                        let lgaId = event.target.value;
                        const formData = new FormData();
                        formData.append('_token', $('input[name=_token]').val());
                        formData.append('id', lgaId);

                        axios.post(this.url.locality, formData)
                            .then((response) => {

                                this.localities = response.data.localities;
                            })
                            .catch( (error) => {

                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            });
                    },
                    copyAsset(asset){


                        var assetLink = this.url.display + asset.slug;

                        const el = document.createElement('textarea');
                        el.value =  assetLink;
                        el.setAttribute('readonly', '');
                        el.style.position = 'absolute';
                        el.style.left = '-9999px';
                        document.body.appendChild(el);
                        el.select();
                        document.execCommand('copy');
                        if(document.execCommand("copy")) {
                            this.$notify({
                                title: 'Success',
                                message: 'Asset link copied',
                                type: 'success'
                            });
                            document.body.removeChild(el);

                        }else{
                            this.$notify.error({
                                title: 'Error',
                                message: 'Unable to copy Asset link'
                            });
                            document.body.removeChild(el);

                        }
                    },
                    getImage(land){
                        if(land.asset.asset_images[0]){
                            return land.asset.asset_images[0].src
                        }else{
                            return this.defaultImage;
                        }
                    }
                }
            });
        }
    </script>

@endsection

