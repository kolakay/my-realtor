<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Assets Manager</title>
        <!-- core:css -->
        <link rel="stylesheet" href="{{ asset('admin-assets/vendors/core/core.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- endinject -->
        <!-- plugin css for this page -->
        <!-- end plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="{{ asset('admin-assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('admin-assets/fonts/feather-font/css/iconfont.css') }}">
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="{{ asset('admin-assets/css/demo_1/style.css') }}">
        <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
        @yield('page-styles')
        <!-- End layout styles -->
        <link rel="icon" href="{{ asset('front-assets/images/favicon.ico') }}" type="image/x-icon">
    </head>
    <body>
        <style>
            .loader{
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: #FFF;
                z-index: 9999999999;
                opacity: 0.89;
            }
            .loader__figure{
                position: absolute;
                top: 50%;
                left: 50%;
            }
        </style>
	    <div class="main-wrapper">

            <!-- partial:../../partials/_sidebar.html -->
            <nav class="sidebar">
                <div class="sidebar-header">
                    <a href="#" class="sidebar-brand">
                    E-PRO<span>AGENT</span>
                    </a>
                    <div class="sidebar-toggler not-active">
                    <span></span>
                    <span></span>
                    <span></span>
                    </div>
                </div>
                <div class="sidebar-body">
                    <ul class="nav">
                        <li class="nav-item nav-category">Main</li>
                        <li class="nav-item {{ ($page == "dashboard")? "active":"" }}">
                            <a href="{{ route('admin.dashboard') }}" class="nav-link">
                            <i class="link-icon" data-feather="rss"></i>
                            <span class="link-title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item nav-category">Asset Manager</li>
                        <li class="nav-item {{ ($page == "lands" || $page == "apartments" || $page == "commercialProperties" || $page == "eventCenters")? "active":"" }}">
                            <a class="nav-link" data-toggle="collapse" href="#emails" role="button" aria-expanded="false" aria-controls="emails">
                                <i class="link-icon" data-feather="home"></i>
                                <span class="link-title">Assets</span>
                                <i class="link-arrow" data-feather="chevron-down"></i>
                            </a>
                            <div class="collapse {{ ($page == "lands" || $page == "apartments" || $page == "commercialProperties" || $page == "eventCenters")? "show":"" }}" id="emails">
                                <ul class="nav sub-menu">
                                    <li class="nav-item {{ ($page == "apartments")? "active":"" }}">
                                        <a href="{{ route('admin.assets.apartments') }}" class="nav-link {{ ($page == "apartments")? "active":"" }}">Apartments</a>
                                    </li>
                                    <li class="nav-item {{ ($page == "commercialProperties")? "active":"" }}">
                                        <a href="{{ route('admin.assets.commercialProperties') }}" class="nav-link {{ ($page == "commercialProperties")? "active":"" }}">Commercial Properties</a>
                                    </li>
                                    <li class="nav-item {{ ($page == "eventCenters")? "active":"" }}">
                                        <a href="{{ route('admin.assets.eventCenters') }}" class="nav-link {{ ($page == "eventCenters")? "active":"" }}">Event Centers</a>
                                    </li>
                                    <li class="nav-item {{ ($page == "lands")? "active":"" }}">
                                        <a href="{{ route('admin.assets.lands') }}" class="nav-link {{ ($page == "lands")? "active":"" }}">Lands</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        {{-- <li class="nav-item {{ ($page == "customers" || $page == "orders" || $page == "subscriptions")? "active":"" }}">
                            <a class="nav-link" data-toggle="collapse" href="#activity" role="button" aria-expanded="false" aria-controls="emails">
                                <i class="link-icon" data-feather="bar-chart-2"></i>
                                <span class="link-title">Activity</span>
                                <i class="link-arrow" data-feather="chevron-down"></i>
                            </a>
                            <div class="collapse {{ ($page == "customers" || $page == "orders" || $page == "subscriptions")? "show":"" }}" id="activity">
                                <ul class="nav sub-menu">
                                    <li class="nav-item {{ ($page == "customers")? "active":"" }}">
                                        <a href="{{ route('user.activity.customers.index') }}" class="nav-link {{ ($page == "customers")? "active":"" }}">Customers</a>
                                    </li>
                                    <li class="nav-item {{ ($page == "orders")? "active":"" }}">
                                        <a href="{{ route('user.activity.orders.index') }}" class="nav-link {{ ($page == "orders")? "active":"" }}">Orders</a>
                                    </li>
                                    <li class="nav-item {{ ($page == "subscriptions")? "active":"" }}">
                                        <a href="{{ route('user.activity.subscriptions.index') }}" class="nav-link {{ ($page == "subscriptions")? "active":"" }}">Subscriptions</a>
                                    </li>
                                </ul>
                            </div>
                        </li> --}}
                        {{-- <li class="nav-item nav-category">Store Management</li>
                        <li class="nav-item {{ ($page == "general" || $page == "processors" ||
                                                 $page == "integrations" || $page == "email" ||
                                                 $page == "custom" || $page == "users")? "active":"" }}">
                            <a class="nav-link" data-toggle="collapse" href="#store" role="button" aria-expanded="false" aria-controls="emails">
                                <i class="link-icon" data-feather="settings"></i>
                                <span class="link-title">Store Settings</span>
                                <i class="link-arrow" data-feather="chevron-down"></i>
                            </a>
                            <div class="collapse {{ ($page == "general" || $page == "processors" ||
                            $page == "integrations" || $page == "email" ||
                            $page == "custom" || $page == "users")? "show":"" }}" id="store">
                                <ul class="nav sub-menu">
                                   <li class="nav-item {{ ($page == "processors")? "active":"" }}">
                                            <a href="{{ route('user.store.settings.processors') }}" class="nav-link {{ ($page == "processors")? "active":"" }}">Processors</a>
                                        </li>
                                    <li class="nav-item {{ ($page == "integrations")? "active":"" }}">
                                        <a href="{{ route('user.store.settings.integrations') }}" class="nav-link {{ ($page == "integrations")? "active":"" }}">Integrations</a>
                                    </li>
                                </ul>
                            </div>
                        </li> --}}
                        <li class="nav-item nav-category">Account Settings</li>
                        <li class="nav-item {{ ($page == "settings")? "active":"" }}">
                            <a href="{{ route('admin.settings') }}" class="nav-link">
                                <i class="link-icon" data-feather="settings"></i>
                                <span class="link-title">Settings</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.logout') }}" class="nav-link">
                                <i class="link-icon" data-feather="lock"></i>
                                <span class="link-title">Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            {{-- <nav class="settings-sidebar">
                <div class="sidebar-body">
                    <a href="#" class="settings-sidebar-toggler">
                        <i data-feather="settings"></i>
                    </a>
                    <h6 class="text-muted">Sidebar:</h6>
                    <div class="form-group border-bottom">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarLight" value="sidebar-light" checked>
                            Light
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarDark" value="sidebar-dark">
                            Dark
                            </label>
                        </div>
                    </div>
                    <div class="theme-wrapper">
                        <h6 class="text-muted mb-2">Light Theme:</h6>
                        <a class="theme-item active" href="admin-demo_1/dashboard-one.html">
                            <img src="{{ asset('admin-assets/images/screenshots/light.jpg') }}" alt="light theme">
                        </a>
                        <h6 class="text-muted mb-2">Dark Theme:</h6>
                        <a class="theme-item" href="admin-demo_2/dashboard-one.html">
                            <img src="{{ asset('admin-assets/images/screenshots/dark.jpg') }}" alt="light theme">
                        </a>
                    </div>
                </div>
            </nav> --}}
		    <!-- partial -->

		    <div class="page-wrapper">

                <!-- partial:../../partials/_navbar.html -->
                <nav class="navbar">
                    <a href="#" class="sidebar-toggler">
                        <i data-feather="menu"></i>
                    </a>
                    {{-- <div class="navbar-content">
                        <form class="search-form">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i data-feather="search"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" id="navbarForm" placeholder="Search here...">
                            </div>
                        </form>
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown nav-apps">
                                <a class="nav-link dropdown-toggle" href="#" id="appsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i data-feather="grid"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="appsDropdown">
                                    <div class="dropdown-header d-flex align-items-center justify-content-between">
                                        <p class="mb-0 font-weight-medium">Web Apps</p>
                                        <a href="javascript:;" class="text-muted">Edit</a>
                                    </div>
                                    <div class="dropdown-body">
                                        <div class="d-flex align-items-center apps">
                                            <a href="../../pages/apps/chat.html"><i data-feather="message-square" class="icon-lg"></i><p>Chat</p></a>
                                            <a href="../../pages/apps/calendar.html"><i data-feather="calendar" class="icon-lg"></i><p>Calendar</p></a>
                                            <a href="../../pages/email/inbox.html"><i data-feather="mail" class="icon-lg"></i><p>Email</p></a>
                                            <a href="../../pages/general/profile.html"><i data-feather="instagram" class="icon-lg"></i><p>Profile</p></a>
                                        </div>
                                    </div>
                                    <div class="dropdown-footer d-flex align-items-center justify-content-center">
                                        <a href="javascript:;">View all</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown nav-messages">
                                <a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i data-feather="mail"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="messageDropdown">
                                    <div class="dropdown-header d-flex align-items-center justify-content-between">
                                        <p class="mb-0 font-weight-medium">9 New Messages</p>
                                        <a href="javascript:;" class="text-muted">Clear all</a>
                                    </div>
                                    <div class="dropdown-body">
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="figure">
                                                <img src="{{ asset('admin-assets/images/faces/face2.jpg') }}" alt="userr">
                                            </div>
                                            <div class="content">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <p>Leonardo Payne</p>
                                                    <p class="sub-text text-muted">2 min ago</p>
                                                </div>
                                                <p class="sub-text text-muted">Project status</p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="figure">
                                                <img src="{{ asset('admin-assets/images/faces/face3.jpg') }}" alt="userr">
                                            </div>
                                            <div class="content">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <p>Carl Henson</p>
                                                    <p class="sub-text text-muted">30 min ago</p>
                                                </div>
                                                <p class="sub-text text-muted">Client meeting</p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="figure">
                                                <img src="{{ asset('admin-assets/images/faces/face4.jpg') }}" alt="userr">
                                            </div>
                                            <div class="content">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <p>Jensen Combs</p>
                                                    <p class="sub-text text-muted">1 hrs ago</p>
                                                </div>
                                                <p class="sub-text text-muted">Project updates</p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="figure">
                                                <img src="{{ asset('admin-assets/images/faces/face5.jpg') }}" alt="userr">
                                            </div>
                                            <div class="content">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <p>Amiah Burton</p>
                                                    <p class="sub-text text-muted">2 hrs ago</p>
                                                </div>
                                                <p class="sub-text text-muted">Project deadline</p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="figure">
                                                <img src="{{ asset('admin-assets/images/faces/face6.jpg') }}" alt="userr">
                                            </div>
                                            <div class="content">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <p>Yaretzi Mayo</p>
                                                    <p class="sub-text text-muted">5 hr ago</p>
                                                </div>
                                                <p class="sub-text text-muted">New record</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="dropdown-footer d-flex align-items-center justify-content-center">
                                        <a href="javascript:;">View all</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown nav-notifications">
                                <a class="nav-link dropdown-toggle" href="#" id="notificationDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i data-feather="bell"></i>
                                    <div class="indicator">
                                        <div class="circle"></div>
                                    </div>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="notificationDropdown">
                                    <div class="dropdown-header d-flex align-items-center justify-content-between">
                                        <p class="mb-0 font-weight-medium">6 New Notifications</p>
                                        <a href="javascript:;" class="text-muted">Clear all</a>
                                    </div>
                                    <div class="dropdown-body">
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="icon">
                                                <i data-feather="user-plus"></i>
                                            </div>
                                            <div class="content">
                                                <p>New customer registered</p>
                                                <p class="sub-text text-muted">2 sec ago</p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="icon">
                                                <i data-feather="gift"></i>
                                            </div>
                                            <div class="content">
                                                <p>New Order Recieved</p>
                                                <p class="sub-text text-muted">30 min ago</p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="icon">
                                                <i data-feather="alert-circle"></i>
                                            </div>
                                            <div class="content">
                                                <p>Server Limit Reached!</p>
                                                <p class="sub-text text-muted">1 hrs ago</p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="icon">
                                                <i data-feather="layers"></i>
                                            </div>
                                            <div class="content">
                                                <p>Apps are ready for update</p>
                                                <p class="sub-text text-muted">5 hrs ago</p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item">
                                            <div class="icon">
                                                <i data-feather="download"></i>
                                            </div>
                                            <div class="content">
                                                <p>Download completed</p>
                                                <p class="sub-text text-muted">6 hrs ago</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="dropdown-footer d-flex align-items-center justify-content-center">
                                        <a href="javascript:;">View all</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown nav-profile">
                                <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{ asset('admin-assets/images/faces/face1.jpg') }}" alt="profile">
                                </a>
                                <div class="dropdown-menu" aria-labelledby="profileDropdown">
                                    <div class="dropdown-header d-flex flex-column align-items-center">
                                        <div class="figure mb-3">
                                            <img src="{{ asset('admin-assets/images/faces/face1.jpg') }}" alt="">
                                        </div>
                                        <div class="info text-center">
                                            <p class="name font-weight-bold mb-0">Amiah Burton</p>
                                            <p class="email text-muted mb-3">amiahburton@gmail.com</p>
                                        </div>
                                    </div>
                                    <div class="dropdown-body">
                                        <ul class="profile-nav p-0 pt-3">
                                            <li class="nav-item">
                                                <a href="../../pages/general/profile.html" class="nav-link">
                                                    <i data-feather="user"></i>
                                                    <span>Profile</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:;" class="nav-link">
                                                    <i data-feather="edit"></i>
                                                    <span>Edit Profile</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:;" class="nav-link">
                                                    <i data-feather="repeat"></i>
                                                    <span>Switch User</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:;" class="nav-link">
                                                    <i data-feather="log-out"></i>
                                                    <span>Log Out</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div> --}}
                </nav>
                <!-- partial -->

                @yield('content')

                <!-- partial:../../partials/_footer.html -->
                <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <p class="text-muted text-center text-md-left">Copyright © 2019 </p>
                    {{-- <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p> --}}
                </footer>
                <!-- partial -->

            </div>
	    </div>

        <!-- core:js -->
        <script src="{{ asset('admin-assets/vendors/core/core.js') }}"></script>
        <script src="{{ asset('admin-assets/vendors/datatables.net/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('admin-assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
        <!-- endinject -->
        <!-- plugin js for this page -->
        <!-- end plugin js for this page -->
        <!-- inject:js -->
        <script src="{{ asset('admin-assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('admin-assets/vendors/feather-icons/feather.min.js') }}"></script>
        <script src="{{ asset('admin-assets/js/template.js') }}"></script>
        <script src="{{ asset('admin-assets/js/chat.js') }}"></script>

        <script src="{{ asset('admin-assets/js/datepicker.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
        <script src="{{ asset('js/axios.js') }}"></script>
        <script src="{{ asset('js/vue.js') }}"></script>
        <script src="https://unpkg.com/element-ui/lib/index.js"></script>
        <script src="{{ asset('admin-assets/js/data-table.js') }}"></script>

        @yield('page-scripts')

        <!-- endinject -->
        <!-- custom js for this page -->
        <!-- end custom js for this page -->
    </body>
</html>
