<div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="chats-tab">
    <form class="form-horizontal commercialProperty-details col-12">
        <div class="form-group row">
            <label for="commercialProperty_status" class="col-sm-3 col-form-label">Commercial Property Status</label>
            <div class="col-sm-8">
                <select class="form-control mb-4" v-model="details.asset.status">
                    <option value="active">Active</option>
                    <option value="in_active">In-Active</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="commercialProperty_title" class="col-sm-3 col-form-label">Commercial Property Title</label>
            <div class="col-sm-8">
                <input type="text"
                    v-model="details.asset.title"
                    id="commercialProperty_title"
                    name="commercialProperty_title"
                    class="form-control" placeholder="Enter commercialProperty Title">
            </div>
        </div>
        <div class="form-group form-float row">
            <label class="col-sm-3 col-form-label">Sale Type</label>
            <div class="col-sm-8">
                <select class="form-control" v-model="details.asset.sale_type">
                    <option value="sale">Sale</option>
                    <option value="rent">Rent</option>
                    <option value="book">Book</option>
                    <option value="lease">Lease</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="internal_commercialProperty_name" class="col-sm-3 col-form-label">Commercial Property Price</label>
            <div class="col-sm-8">
                    <input type="text"
                        v-model="details.asset.price"
                        name="asset_price" class="form-control"
                        placeholder="commercialProperty Price">

            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-3 col-form-label">Commercial Property Description</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <textarea rows="4"
                        v-model="details.asset.description"
                        class="form-control no-resize"
                        name="description" placeholder="Please type what you want..."></textarea>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-3 col-form-label">Apartment Locality</label>
            <div class="col-sm-8">
                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">LGA</label>
                                <select class="form-control" v-model="details.asset.lga_id" @change="getLocality($event)">
                                    <option v-for="lga in lgas" :value="lga.id">@{{ lga.lga }}</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">Locality</label>
                                <select class="form-control" v-model="details.asset.locality_id">
                                    <option v-for="locality in localities" :value="locality.id">@{{ locality.locality }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-3 col-form-label">Commercial Property Address</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <textarea rows="4"
                        v-model="details.asset.address"
                        class="form-control no-resize"
                        name="address" placeholder="Please type what you want..."></textarea>
                </div>
            </div>
        </div>

        <div class="form-group form-float row">
            <label class="col-sm-3 col-form-label">Commercial Property Type</label>
            <div class="col-sm-8">
                <select class="form-control" v-model="details.commercialProperty.type">
                    <option value="factory">Factory</option>
                    <option value="farm">Farm</option>
                    <option value="filling_station">Filling Station</option>
                    <option value="hotel">Hotel</option>
                    <option value="mall">Mall</option>
                    <option value="office">Office</option>
                    <option value="school">School</option>
                    <option value="shop">Shop</option>
                    <option value="warehouse">Warehouse</option>
                </select>
            </div>
        </div>
        <div class="form-group form-float row">
            <label class="col-sm-3 col-form-label">File upload</label>
            <div class="col-sm-8">
                <input type="file" name="img[]" class="file-upload-default">
                <div class="input-group col-xs-12">
                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                    <span class="input-group-append">
                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="commercialProperty_square_meters" class="col-sm-3 col-form-label">Commercial Property Square Meters</label>
            <div class="col-sm-8">
                    <input type="number"
                        v-model="details.commercialProperty.square_meters"
                        name="commercialProperty_square_meters" class="form-control"
                        placeholder="commercialProperty Square Meters">

            </div>
        </div>
        <div class="form-group row">
            <label for="commercialProperty_parking_space" class="col-sm-3 col-form-label">Commercial Property Parking Space</label>
            <div class="col-sm-8">
                    <input type="number"
                        v-model="details.commercialProperty.parking_space"
                        name="commercialProperty_parking_space" class="form-control"
                        placeholder="commercialProperty Parking Space">

            </div>
        </div>

    </form>
</div>
