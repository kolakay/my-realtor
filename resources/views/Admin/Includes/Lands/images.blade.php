<div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
    <div class="form-group form-float row m-2">
        <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-success" onclick="$('#pro-image').click()">Upload Images</a>
            <input type="file" name="images" multiple style="display: none;" id="pro-image"  v-on:change="readImage">
            <div class="col-xs-8 mx-auto border p-2 mt-2">
                <div class="latest-photos">
                    <div class="row preview-images-zone">
                        <div class="col-md-1 preview-image" v-for="image in images">
                            <div class="image-cancel">
                                <button type="button" @click="deleteImage(image.id)" class="icon btn btn-sm btn-danger btn-icon mr-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash icon-lg mr-0"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                </button>
                            </div>
                            <figure class="image-zone">
                                <img class="img-fluid" width="100" height="1oo" :src="image.src" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
