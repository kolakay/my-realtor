<div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="chats-tab">
    <form class="form-horizontal apartment-details col-12">
        <div class="form-group row">
            <label for="apartment_status" class="col-sm-3 col-form-label">Apartment Status</label>
            <div class="col-sm-8">
                <select class="form-control mb-4" v-model="details.asset.status">
                    <option value="active">Active</option>
                    <option value="in_active">In-Active</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="apartment_title" class="col-sm-3 col-form-label">Apartment Title</label>
            <div class="col-sm-8">
                <input type="text"
                    v-model="details.asset.title"
                    id="apartment_title"
                    name="apartment_title"
                    class="form-control" placeholder="Enter Apartment Title">
            </div>
        </div>
        <div class="form-group form-float row">
            <label class="col-sm-3 col-form-label">Sale Type</label>
            <div class="col-sm-8">
                <select class="form-control" v-model="details.asset.sale_type">
                    <option value="sale">Sale</option>
                    <option value="rent">Rent</option>
                    <option value="book">Book</option>
                    <option value="lease">Lease</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="internal_apartment_name" class="col-sm-3 col-form-label">Apartment Price</label>
            <div class="col-sm-8">
                    <input type="text"
                        v-model="details.asset.price"
                        name="asset_price" class="form-control"
                        placeholder="Apartment Price">

            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-3 col-form-label">Apartment Description</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <textarea rows="4"
                        v-model="details.asset.description"
                        class="form-control no-resize"
                        name="description" placeholder="Please type what you want..."></textarea>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-3 col-form-label">Apartment Locality</label>
            <div class="col-sm-8">
                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">LGA</label>
                                <select class="form-control" v-model="details.asset.lga_id" @change="getLocality($event)">
                                    <option v-for="lga in lgas" :value="lga.id">@{{ lga.lga }}</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">Locality</label>
                                <select class="form-control" v-model="details.asset.locality_id">
                                    <option v-for="locality in localities" :value="locality.id">@{{ locality.locality }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-sm-3 col-form-label">Apartment Address</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <textarea rows="4"
                        v-model="details.asset.address"
                        class="form-control no-resize"
                        name="address" placeholder="Please type what you want..."></textarea>
                </div>
            </div>
        </div>

        <div class="form-group form-float row">
            <label class="col-sm-3 col-form-label">Apartment Type</label>
            <div class="col-sm-8">
                <select class="form-control" v-model="details.apartment.type">
                    <option value="house">House</option>
                    <option value="flat">Flat</option>
                    <option value="mini_flat">Mini Flat</option>
                    <option value="bungalow">Bungalow</option>
                    <option value="duplex">Duplex</option>
                    <option value="mansion">Mansion</option>
                    <option value="villa">Villa</option>
                    <option value="pent_house">Pent House</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="apartment_total_rooms" class="col-sm-3 col-form-label">Total Rooms</label>
            <div class="col-sm-8">
                    <input type="number"
                        v-model="details.apartment.total_rooms"
                        name="total_rooms" class="form-control"
                        placeholder="Total Rooms">

            </div>
        </div>

        <div class="form-group row">
            <label for="apartment_bedrooms" class="col-sm-3 col-form-label">Total Bedrooms</label>
            <div class="col-sm-8">
                    <input type="number"
                        v-model="details.apartment.bedrooms"
                        name="bedrooms" class="form-control"
                        placeholder="Total Bedrooms">

            </div>
        </div>

        <div class="form-group row">
            <label for="apartment_bathrooms" class="col-sm-3 col-form-label">Total Bathrooms</label>
            <div class="col-sm-8">
                    <input type="number"
                        v-model="details.apartment.bathrooms"
                        name="apartment_bathrooms" class="form-control"
                        placeholder="Total Bathrooms">

            </div>
        </div>

        <div class="form-group row">
            <label for="apartment_square_meters" class="col-sm-3 col-form-label">Apartment Square Meters</label>
            <div class="col-sm-8">
                    <input type="number"
                        v-model="details.apartment.square_meters"
                        name="apartment_square_meters" class="form-control"
                        placeholder="Apartment Square Meters">

            </div>
        </div>
        <div class="form-group row">
            <label for="apartment_parking_space" class="col-sm-3 col-form-label">Apartment Parking Space</label>
            <div class="col-sm-8">
                    <input type="number"
                        v-model="details.apartment.parking_space"
                        name="apartment_parking_space" class="form-control"
                        placeholder="Apartment Parking Space">

            </div>
        </div>

        <div class="form-group form-float row">
            <label class="form-label col-sm-3">Apartment Furnishing</label>
            <div class="col-sm-8">
                <select class="form-control" v-model="details.apartment.furnishing">
                    <option value="furnished">Furnished</option>
                    <option value="semi_furnished">Semi Furnished</option>
                    <option value="unfurnished">Unfurnished</option>
                </select>
            </div>
        </div>


        {{-- <div class="form-group row">
            <label for="apartment_image" class="col-sm-3 col-form-label">apartment Image</label>
            <div class="col-sm-8">
                <div class="form-group" v-show="details.apartment.image.length < 1">
                    <div class="form-line">
                        <input type="file" name="" id="" class="form-control" v-on:change="onImageUpload">
                    </div>
                </div>
                <div class="form-group col-sm-12 row" v-show="details.apartment.image.length > 1">
                    <div class="form-line col-8">
                        <img :src="details.apartment.apartment_image" alt="" class="img-fluid">
                    </div>
                    <div class="form-line col-2">
                        <button type="button" v-on:click="deleteImage" class="btn btn-danger btn-icon">
                            <i data-feather="trash"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div> --}}

    </form>
</div>
