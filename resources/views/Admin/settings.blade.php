@extends('Admin.Layouts.master')
@section('page-styles')
<link rel="stylesheet" href="{{ asset('admin-assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
{{-- <link href="{{ asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"> --}}
<style>
    #url-container{
        padding-left: 0px;
        padding-right: 30px;
    }
    .eventCenter-details, .type-container{
        margin-top: 30px;
    }
    .eventCenter-edit-container{
        max-height: 700px;
        min-height: 500px;
        overflow-y: scroll;
    }
    .eventCenter-edit-container::-webkit-scrollbar {
        width: 7px;
    }

    /* Track */
    .eventCenter-edit-container::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
        border-radius: 10px;
    }

    /* Handle */
    .eventCenter-edit-container::-webkit-scrollbar-thumb {
        background: #727cf5;
        border-radius: 3px;
    }

    /* Handle on hover */
    .eventCenter-edit-container::-webkit-scrollbar-thumb:hover {
        background: #727cf5;
    }

    .image-cancel{
        display: none;
        position: absolute;
        height: 100px;
        width: 100px;
        background: #FFF;
        text-align: right;
        /* padding-right: 5px; */
        opacity: .8;
        /* cursor: pointer; */
    }

    .preview-image:hover > .image-cancel {
        display: block;
    }

</style>
@endsection
@section('content')
    <div class="page-content" id="app">
        <div class="loader d-flex justify-content-center" v-if="isLoading == true">
            <div class="spinner-border loader__figure" role="status">
                <span class="sr-only">Loading...</span>

            </div>

        </div>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0">Edit User Detail</h4>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row position-relative">
                    <div class="col-12 chat-aside">
                        <div class="aside-content">
                            <div class="aside-body">
                                <ul class="nav nav-tabs mt-3" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">
                                            <div class="d-flex flex-row flex-lg-column flex-xl-row align-items-center">
                                            <i data-feather="settings" class="icon-sm mr-sm-2 mr-lg-0 mr-xl-2 mb-md-1 mb-xl-0"></i>
                                            <p class="d-none d-sm-block">User Details</p>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="images-tab" data-toggle="tab" href="#password" role="tab" aria-controls="images" aria-selected="true">
                                            <div class="d-flex flex-row flex-lg-column flex-xl-row align-items-center">
                                            <i data-feather="settings" class="icon-sm mr-sm-2 mr-lg-0 mr-xl-2 mb-md-1 mb-xl-0"></i>
                                            <p class="d-none d-sm-block">Password</p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <div class="eventCenter-edit-container">
                                    <div class="tab-content mt-3">
                                        @csrf
                                        <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="chats-tab">
                                            <form class="form-horizontal apartment-details col-12">
                                                <div class="form-group row">
                                                    <label for="apartment_title" class="col-sm-3 col-form-label">User name</label>
                                                    <div class="col-sm-8">
                                                        <input type="text"
                                                            v-model="user.name"
                                                            id="user_name"
                                                            name="user_name"
                                                            class="form-control" placeholder="Enter User Name">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="apartment_title" class="col-sm-3 col-form-label">User Email</label>
                                                    <div class="col-sm-8">
                                                        <input type="text"
                                                            v-model="user.email"
                                                            id="user_email"
                                                            name="user_email"
                                                            class="form-control" placeholder="Enter User Email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0" v-on:click="updateUser">
                                                        <i class="btn-icon-prepend" data-feather="download-cloud"></i> Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="chats-tab">
                                            <form class="form-horizontal apartment-details col-12">
                                                <div class="form-group row">
                                                    <label for="apartment_title" class="col-sm-3 col-form-label">Old Password</label>
                                                    <div class="col-sm-8">
                                                        <input type="password"
                                                            v-model="password.old_password"
                                                            id="old_password"
                                                            name="old_password"
                                                            class="form-control" placeholder="Enter Old Password">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="apartment_title" class="col-sm-3 col-form-label">New Password</label>
                                                    <div class="col-sm-8">
                                                        <input type="password"
                                                            v-model="password.password"
                                                            id="password"
                                                            name="password"
                                                            class="form-control" placeholder="Enter New Password">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="apartment_title" class="col-sm-3 col-form-label">Confrim New Password</label>
                                                    <div class="col-sm-8">
                                                        <input type="password"
                                                            v-model="password.password_confirmation"
                                                            id="password_confirmation"
                                                            name="password_confirmation"
                                                            class="form-control" placeholder="Confrim Enter New Password">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0" v-on:click="updatePassword">
                                                        <i class="btn-icon-prepend" data-feather="download-cloud"></i> Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <textarea style="display:none" name="" id="user">{!! json_encode(Auth::user()) !!}</textarea>
    </div>
@endsection
@section('page-scripts')
<script>
    if (window.Vue) {
        const vueApp = new Vue({
            el: '#app',
            data : {
                isLoading: false,
                user : {
                    name: '',
                    email: ''
                },
                password: {
                    old_password: '',
                    password: '',
                    password_confirmation: '',

                },
                url : {
                    updateDetails : `{{ route('admin.settings.update.details') }}`,
                    updatePassword: `{{ route('admin.settings.update.password') }}`
                },
                formErrors : [],


            },
            mounted(){
                this.user = JSON.parse($("#user").val());
            },
            methods : {
                updateUser(){
                    const formData = new FormData();
                    for ( var key in this.user ) {
                        let value = this.user[key];
                        formData.append(key, value);
                    }
                    formData.append('_token', $('input[name=_token]').val());

                    this.isLoading = true;

                    axios.post(this.url.updateDetails, formData)
                        .then((response) => {
                            this.isLoading = false;

                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        })
                        .catch((error) => {
                            this.isLoading = false;

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        });
                },
                updatePassword(){
                    const formData = new FormData();
                    formData.append('id', this.user.id);
                    formData.append('old_password', this.password.old_password);
                    formData.append('password', this.password.password);
                    formData.append('password_confirmation', this.password.password_confirmation);
                    formData.append('_token', $('input[name=_token]').val());

                    this.isLoading = true;

                    axios.post(this.url.updatePassword, formData)
                        .then((response) => {
                            this.isLoading = false;

                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        })
                        .catch((error) => {
                            this.isLoading = false;

                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        });
                }

            }
        });
    }
</script>
@endsection
