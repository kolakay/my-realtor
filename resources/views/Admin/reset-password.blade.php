<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>E-PRO AGENT</title>
	<!-- core:css -->
	<link rel="stylesheet" href="{{ asset('admin-assets/vendors/core/core.css') }}">
	<!-- endinject -->
  <!-- plugin css for this page -->
	<!-- end plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="{{ asset('admin-assets/fonts/feather-font/css/iconfont.css') }}">
	<!-- endinject -->
  <!-- Layout styles -->
	<link rel="stylesheet" href="{{ asset('admin-assets/css/demo_1/style.cs') }}s">
  <!-- End layout styles -->
  <link rel="icon" href="{{ asset('front-assets/images/favicon.ico') }}" type="image/x-icon">
</head>
<body>
	<div class="main-wrapper">
		<div class="page-wrapper full-page">
			<div class="page-content d-flex align-items-center justify-content-center">

				<div class="row w-100 mx-0 auth-page">
					<div class="col-md-8 col-xl-6 mx-auto">
						<div class="card">
							<div class="row">
                                <div class="col-md-4 pr-md-0">
                                    <div class="auth-left-wrapper">

                                    </div>
                                </div>
                                <div class="col-md-8 pl-md-0">
                                    <div class="auth-form-wrapper px-4 py-5">
                                        @include('Admin.Includes.notifications')
                                        {{-- <a href="#" class="noble-ui-logo d-block mb-2">Noble<span>UI</span></a> --}}
                                        <h5 class="text-muted font-weight-normal mb-4"> Enter your email address that you used to register. We'll send you an email with a
                                                link to reset your password.</h5>
                                        <form class="forms-sample" method="POST" action="{{ route('admin.update-password') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="passowrd">Password</label>
                                                <input type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                                                <input type="hidden" name="token" value="{{ $user->token }}" />
                                            </div>
                                                <div class="form-group">
                                                    <label for="passowrd">Password</label>
                                                    <input type="password" class="form-control" name="re-type_password" minlength="6" placeholder="Password" required>
                                                </div>
                                            <div class="mt-3">
                                                <button type="submit" class="btn btn-primary mr-2 mb-2 mb-md-0">Update Password</button>

                                            </div>
                                            <a href="{{ route('admin.login') }}" class="d-block mt-3 text-muted">Login</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- core:js -->
	<script src="{{ asset('admin-assets/vendors/core/core.js') }}"></script>
	<!-- endinject -->
  <!-- plugin js for this page -->
	<!-- end plugin js for this page -->
	<!-- inject:js -->
	<script src="{{ asset('admin-assets/vendors/feather-icons/feather.min.js') }}"></script>
	<script src="{{ asset('admin-assets/js/template.js') }}"></script>
	<!-- endinject -->
  <!-- custom js for this page -->
	<!-- end custom js for this page -->
</body>
</html>
