<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = "My Realtor";
        $user->email = "admin@myrealtor.com";
        $user->password = bcrypt('123456789');
        $user->role = "admin";
        $user->status = "active";
        $user->save();
    }
}
