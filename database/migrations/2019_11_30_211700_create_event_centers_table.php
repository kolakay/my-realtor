<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_centers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('asset_id');
            $table->enum('type', [
                'church', 'conference_center', 'club_hall',
                'gallery', 'gardens', 'marquee',
                'meeting_room', 'multipurpose_hall', 'theatre'
            ]);
            $table->integer('square_meters')->default(0);
            $table->integer('parking_space')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_centers');
    }
}
