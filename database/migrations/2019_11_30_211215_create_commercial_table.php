<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommercialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commercial', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('asset_id');
            $table->enum('type', [
                'office', 'shop', 'hotel',
                'warehouse', 'factory', 'filling_station',
                'mall', 'school', 'farm'
            ]);
            $table->integer('square_meters')->default(0);
            $table->integer('parking_space')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commercial');
    }
}
