<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');

            $table->integer('country_id')->default(160);
            $table->integer('state_id')->default(33);
            $table->integer('lga_id')->nullable();
            $table->integer('locality_id')->nullable();

            $table->enum('sale_type', ['sale', 'rent', 'book', 'lease']);
            $table->enum('category', ['apartments', 'commercial_properties', 'lands', 'event_centers']);
            $table->text('images')->nullable();
            $table->text('address')->nullable();
            $table->text('description')->nullable();
            $table->decimal('price', 28,2)->nullable();
            $table->enum('status', ['active', 'in_active'])->default('in_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
