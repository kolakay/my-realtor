<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'HomeController@index')->name('home.index');
// Route::get('/about', 'HomeController@about')->name('home.about');
Route::get('/contact', 'HomeController@contact')->name('home.contact');
Route::get('/properties', 'HomeController@properties')->name('home.properties');
Route::get('/properties/details/{slug?}', 'HomeController@propertyDetails')->name('home.properties.details');

Route::post('/contact/message', 'HomeController@contactMessage')->name('home.contact.message');

Route::get('/request', 'RequestController@request')->name('home.request');


Route::get('/super-user/login', 'Admin\AuthController@signIn')->name('admin.login');
Route::get('/super-user/logout', 'Admin\AuthController@logout')->name('admin.logout');
Route::post('/super-user/login', 'Admin\AuthController@login')->name('admin.login.account');
Route::get('/super-user/passoword/recovery', 'Admin\AuthController@forgotPassword')->name('admin.password.recover');
Route::post('/super-user/passoword/recovery', 'Admin\AuthController@verifyEmail')->name('admin.password.verfiy-email');
Route::get('/super-user/reset-password/{token}', 'Admin\AuthController@resetPassword')->name('admin.reset-password');
Route::post('/super-user/reset-password', 'Admin\AuthController@updatePassword')->name('admin.update-password');

Route::get('/super-user/settings', 'Admin\SettingsController@index')->name('admin.settings');
Route::post('/super-user/settings/update/password', 'Admin\SettingsController@updatePassword')->name('admin.settings.update.password');
Route::post('/super-user/settings/update/details', 'Admin\SettingsController@updateDetails')->name('admin.settings.update.details');


Route::get('/super-user', 'Admin\IndexController@index')->name('admin.dashboard');

Route::get('/super-user/assets/apartments', 'Admin\ApartmentsController@apartments')->name('admin.assets.apartments');
Route::get('/super-user/assets/apartments/edit/{productId?}', 'Admin\ApartmentsController@editApartment')->name('admin.assets.apartments.edit');
Route::get('/super-user/assets/apartments/delete/{productId?}', 'Admin\ApartmentsController@deleteApartment')->name('admin.assets.apartments.delete');

Route::post('/super-user/assets/apartments/create', 'Admin\ApartmentsController@addApartment')->name('admin.assets.apartments.add');
Route::post('/super-user/assets/apartments/update', 'Admin\ApartmentsController@updateApartment')->name('admin.assets.apartments.update');
Route::post('/super-user/assets/apartments/images/upload', 'Admin\ApartmentsController@saveImages')->name('admin.assets.apartments.images.upload');
Route::post('/super-user/assets/apartments/image/delete', 'Admin\ApartmentsController@deleteImage')->name('admin.assets.apartments.image.delete');

Route::get('/super-user/assets/commercial-properties', 'Admin\CommercialController@index')->name('admin.assets.commercialProperties');
Route::get('/super-user/assets/commercial-properties/edit/{productId?}', 'Admin\CommercialController@editCommercialProperty')->name('admin.assets.commercialProperties.edit');
Route::get('/super-user/assets/commercial-properties/delete/{productId?}', 'Admin\CommercialController@deleteCommercialProperty')->name('admin.assets.commercialProperties.delete');
Route::post('/super-user/assets/commercial-properties/create', 'Admin\CommercialController@addCommercialProperty')->name('admin.assets.commercialProperties.add');
Route::post('/super-user/assets/commercial-properties/update', 'Admin\CommercialController@updateCommercialProperty')->name('admin.assets.commercialProperties.update');
Route::post('/super-user/assets/commercial-properties/images/upload', 'Admin\CommercialController@saveImages')->name('admin.assets.commercialProperties.images.upload');
Route::post('/super-user/assets/commercial-properties/image/delete', 'Admin\CommercialController@deleteImage')->name('admin.assets.commercialProperties.image.delete');


Route::get('/super-user/assets/event', 'Admin\EventCentersController@index')->name('admin.assets.eventCenters');
Route::get('/super-user/assets/event-centers/edit/{productId?}', 'Admin\EventCentersController@editEventCenter')->name('admin.assets.eventCenters.edit');
Route::get('/super-user/assets/event-centers/delete/{productId?}', 'Admin\EventCentersController@deleteEventCenter')->name('admin.assets.eventCenters.delete');
Route::post('/super-user/assets/event-centers/create', 'Admin\EventCentersController@addEventCenter')->name('admin.assets.eventCenters.add');
Route::post('/super-user/assets/event-centers/update', 'Admin\EventCentersController@updateEventCenter')->name('admin.assets.eventCenters.update');
Route::post('/super-user/assets/event-centers/images/upload', 'Admin\EventCentersController@saveImages')->name('admin.assets.eventCenters.images.upload');
Route::post('/super-user/assets/event-centers/image/delete', 'Admin\EventCentersController@deleteImage')->name('admin.assets.eventCenters.image.delete');



Route::get('/super-user/assets/lands', 'Admin\LandsController@index')->name('admin.assets.lands');
Route::get('/super-user/assets/lands/edit/{productId?}', 'Admin\LandsController@editLandProperty')->name('admin.assets.lands.edit');
Route::get('/super-user/assets/lands/delete/{productId?}', 'Admin\LandsController@deleteLandProperty')->name('admin.assets.lands.delete');
Route::post('/super-user/assets/lands/create', 'Admin\LandsController@addLandProperty')->name('admin.assets.lands.add');
Route::post('/super-user/assets/lands/update', 'Admin\LandsController@updateLandProperty')->name('admin.assets.lands.update');
Route::post('/super-user/assets/lands/images/upload', 'Admin\LandsController@saveImages')->name('admin.assets.lands.images.upload');
Route::post('/super-user/assets/lands/image/delete', 'Admin\LandsController@deleteImage')->name('admin.assets.lands.image.delete');


Route::post('/super-user/assets/localities/fetch', 'Admin\AssetController@fetchLocalities')->name('admin.assets.localities');
