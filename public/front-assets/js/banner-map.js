

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
		scrollwheel: false,
		
		styles: [
            {elementType: 'geometry', stylers: [{color: '#e6e8e9'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: 'rgba(0,0,0,0)'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#666666'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#666666'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#666666'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#e6e8e9'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#666666'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#a6b9c7'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: 'rgba(0,0,0,0)'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#a6b9c7'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#e6e8e9'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#becdd9'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#222222'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#666666'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#bfd3e4'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#666666'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: 'rgba(0,0,0,0)'}]
            }
          ]
	};
	
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
        ['Connecticut', 41.640055, -72.356191],
        ['New Hampshire', 43.316403, -71.702790],
		['Massachusetts', 42.536869, -72.092519]
    ];
                        
    // Info Window Content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h3>Connecticut</h3>' +
        '<p>Put Your Content Here</p>' +        '</div>'],
        ['<div class="info_content">' +
        '<h3>New Hampshire</h3>' +
        '<p>Put Your Content Here</p>' +        '</div>'],
		['<div class="info_content">' +
        '<h3>Massachusetts</h3>' +
        '<p>Put Your Content Here</p>' +        '</div>']
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
			icon: 'images/icons/map-marker.png'
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(8);
        google.maps.event.removeListener(boundsListener);
    });
    
}