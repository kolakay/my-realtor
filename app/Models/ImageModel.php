<?php

namespace App\Models;

use Storage;
use App\Helpers\Paths;

use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model
{
    protected $table = "images";

    public function asset(){
		return $this->belongsTo('App\Models\AssetModel','asset_id','id');
    }

    public function getSrcAttribute($value){
        $assetImage = Paths::ASSET_PATH .$value;
        if(Storage::has($assetImage)){
            $image = \Request::root().Storage::disk('local')->url($assetImage);
            return $image;

        }

        return asset('admin-assets/images/placeholder.jpg');
    }
}
