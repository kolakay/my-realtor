<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApartmentModel extends Model
{
    protected $table = "apartments";

    protected $with = ['asset'];

    protected $casts = [
        'features' => 'array'
    ];

    protected $appends = [
        'asset_images',
    ];

    public function asset(){
		return $this->belongsTo('App\Models\AssetModel','asset_id','id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ImageModel', 'asset_id', 'asset_id')->select(['id', 'src']);
    }

    public function getAssetImagesAttribute(){
        return $this->images();
    }
}
