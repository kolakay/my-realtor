<?php

namespace App\Models;
use Exception, Log;
use App\Models\ImageModel;
use Illuminate\Database\Eloquent\Model;

class AssetModel extends Model
{
    protected $table = "assets";

    protected $casts = [
        'images' => 'array'
    ];

    protected $appends = [
        'asset_location',
        'formatted_price',
        'first_image'
    ];

    // protected $with = ['assetImages'];

    public function getFormattedPriceAttribute(){
        return number_format($this->price);
    }

    public function apartment(){
        return $this->hasOne('App\Models\ApartmentModel','asset_id','id');
    }

    public function commercialProperty(){
        return $this->hasOne('App\Models\CommercialPropertyModel','asset_id','id');
    }

    public function eventCenter(){
        return $this->hasOne('App\Models\EventCenterModel','asset_id','id');
    }

    public function landProperty(){
        return $this->hasOne('App\Models\LandPropertyModel','asset_id','id');
    }

    public function country(){
		return $this->belongsTo('App\Models\CountryModel','country_id','id');
    }

    public function state(){
		return $this->belongsTo('App\Models\StateModel','state_id','id');
    }

    public function lga(){
		return $this->belongsTo('App\Models\LGAModel','lga_id','id');
    }

    public function locality(){
		return $this->belongsTo('App\Models\LocalityModel','locality_id','id');
    }

    public function assetImages()
    {
        return $this->hasMany('App\Models\ImageModel', 'asset_id', 'id')->select(['id', 'src']);
    }

    public function getFirstImageAttribute(){
        try{
            $image = ImageModel::where('asset_id', $this->id)->first();
            if($image){
                return $image->src;
            }else{
                return asset('admin-assets/images/placeholder.jpg');
            }

        }catch(Exception $error){
            Log::info('AssetModel@getFirstImageAttribute error message: ' . $error->getMessage());
            return asset('admin-assets/images/placeholder.jpg');
        }


    }

    public function getAssetLocationAttribute(){
        $locality = '';
        $lga = '';
        $state = '';
        if($this->locality){
            $locality = $this->locality->locality;
        }

        if($this->lga){
            $lga = $this->lga->lga;
        }

        if($this->state){
            $state = $this->state->state;
        }

        $location = $locality.", ". $lga.", ". $state;

        return $location;
    }
}
