<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommercialPropertyModel extends Model
{
    protected $table = "commercial";

    protected $with = ['asset'];

    public function asset(){
		return $this->belongsTo('App\Models\AssetModel','asset_id','id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ImageModel', 'asset_id', 'asset_id')->select(['id', 'src']);
    }
}
