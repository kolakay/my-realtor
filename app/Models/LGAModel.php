<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LGAModel extends Model
{
    protected $table = "lgas";

    public function state(){
		return $this->belongsTo('App\Models\StateModel','state_id','id');
    }
}
