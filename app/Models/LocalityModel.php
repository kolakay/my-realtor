<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocalityModel extends Model
{
    protected $table = "localities";

    public function lga(){
		return $this->belongsTo('App\Models\LGAModel','lga_id','id');
    }
}
