<?php

namespace App\Http\Controllers;

use App\Helpers\Paths;
use Exception, Log, Validator, Session,  Storage, File, Mail;
use App\Models\AssetModel;
use App\Models\ContactModel;
use App\Mail\ContactUsMail;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function request(){
        try{
            $seoAsset= AssetModel::all()->random(1)->first();
            $data = [
                'seoAsset' => $seoAsset,
                'page' => 'request'
            ];
            return view('Front.request', $data);
        }catch(Exception $error){

        }
    }
}
