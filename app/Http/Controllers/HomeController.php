<?php

namespace App\Http\Controllers;

use App\Helpers\Paths;
use Exception, Log, Validator, Session,  Storage, File, Mail;
use App\Models\AssetModel;
use App\Models\ContactModel;
use App\Mail\ContactUsMail;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        try{

            // $assets = AssetModel::all();
            $featuredAssets = AssetModel::all()->random(3);
            $seoAsset= AssetModel::all()->random(1)->first();
            $assets = AssetModel::where('status', 'active')
                ->orderBy('created_at', 'DESC')
                ->get()->take(6);
            $data = [
                'page' => 'index',
                'assets' => $assets,
                'seoAsset' => $seoAsset,
                'featuredAssets' => $featuredAssets
            ];

            return view('Front.index', $data);
        }catch(Exception $error){

        }
    }

    public function contact(){
        try{
            $seoAsset= AssetModel::all()->random(1)->first();
            $data = [
                'seoAsset' => $seoAsset,
                'page' => 'contact'
            ];
            return view('Front.contact', $data);
        }catch(Exception $error){

        }
    }

    public function about(){
        try{
            $seoAsset= AssetModel::all()->random(1)->first();

            $data = [
                'seoAsset' => $seoAsset,
                'page' => 'about'
            ];
            return view('Front.about', $data);
        }catch(Exception $error){

        }
    }

    public function properties(){
        try{
            $seoAsset= AssetModel::all()->random(1)->first();

            $assets = AssetModel::paginate(10);
            $featuredAssets = AssetModel::all()->random(4);
            $data = [
                'assets' => $assets,
                'seoAsset' => $seoAsset,
                'page' => 'properties',
                'featuredAssets' => $featuredAssets
            ];
            return view('Front.properties', $data);
        }catch(Exception $error){

        }
    }

    public function propertyDetails($slug){
        try{
            $asset = AssetModel::where('slug', $slug)->first();
            if(!$asset){
                abort(404);
            }
            $featuredAssets = AssetModel::all()->random(4);

            $data = [
                'asset' => $asset,
                'page' => 'properties',
                'featuredAssets' => $featuredAssets
            ];
            return view('Front.property-details', $data);
        }catch(Exception $error){

        }
    }

    public function contactMessage(Request $request){
        try{
            if(!$request->name || ! $request->email || !$request->message){
                $message = 'Please your contact details and message are required.';
                return $this->handleAjaxError($message, 400);
            }
            $contact = new ContactModel;
            $contact->name = $request->name;
            $contact->email = $request->email;
            $contact->subject = $request->subject;
            $contact->price_start = $request->price_start;
            $contact->price_end = $request->price_end;
            $contact->phone = $request->phone;
            $contact->message = $request->message;

            $contact->save();
            $this->dispatchMails($contact);


            $message = "Message recieved. we'll get back to you as soon as we can.";
            // Session::put('successMessage', $message);
            // return redirect()->back();
            return response()->json([
                'status' => 'success',
                'message'=> $message
            ]);

        }catch(Exception $error){
            Log::info('HomeController@contactMessage error message: ' . $error->getMessage());
            $message = 'Unable to store Assets. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    private function dispatchMails(ContactModel $contact){
        try{
            Mail::to('kolakachi@eproagent.com')
                    ->send(new ContactUsMail($contact));
            Mail::to('chibuzor@eproagent.com')
                    ->send(new ContactUsMail($contact));
            // Mail::to('lazarushart@eproagent.com')
            //         ->send(new ContactUsMail($contact));

            return true;
        }catch(\Exception $error){
            Log::info('error message: ' . $error->getMessage());

            return false;
        }
    }

    private function handleAjaxError($message, $errorCode, $errors = []){
        return response()->json([
            'status' => 'error',
            'errors' => $errors,
            'message' => $message
        ], $errorCode);
    }
}
