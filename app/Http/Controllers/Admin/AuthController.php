<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Support\Str;
use App\Mail\PasswordRecoveryMail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log, Mail, Auth, Validator, Session, Exception;


class AuthController extends Controller
{
    public function signIn(){
        return view('Admin.login');
    }

    public function login(Request $request){
        try{
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
                return redirect()->route('admin.dashboard');
            }else{

                $message = "Wrong email or password";
                Session::put('errorMessage', $message);

                return redirect()->back();
            }


        }catch(Exception $error){
            Log::info('AuthController@getPayPalProductDetails--error message: ' . $error->getMessage());
            $message = "Unable to complete request. Encountered an error";
            return $this->returnResponseWithErrorMessage($message);
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login');
    }

    public function forgotPassword(){
        return view('Admin.forgot-password');
    }

    public function verifyEmail(Request $request){
        try{
            $validator =  $this->validator($request->all());

            if($validator->fails()){
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $tokenSent = $this->tokenSentToUserMail($request);

            if($tokenSent){
                $message = 'we have sent a password reset link to your mail';
                Session::put('successMessage', $message);
                return redirect()->back();
            }else{
                $message = 'Please try again';
                return $this->returnResponseWithErrorMessage($message);

            }

        }catch(Exception $error){
            Log::info('AuthController@verifyEmail--error message: ' . $error->getMessage());
            $message = "Unable to complete request. Encountered an error";
            return $this->returnResponseWithErrorMessage($message);
        }
    }

    public function resetPassword($token){
        try{
            $user = User::where('token', $token)->first();

            if($user){
                $data = array('user'=>$user);
                return view('Admin.reset-password', $data);
            }else{
                return redirect()->redirectToRoute('auth.login');
            }
        }catch(\Exception $error){
            Log::info('Admin\AuthController@resetPassword error message: ' . $error->getMessage());

            $message = 'Unable to complete request, please try again';
            return $this->returnResponseWithErrorMessage($message);
        }
    }

    public function updatePassword(Request $request){
        try{
            $validator =  $this->resetValidator($request->all());

            if($validator->fails()){
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $user = User::where('token', $request->input('token'))->first();

            if($user){

                $user->password = bcrypt($request->input('password'));
                $user->token = null;
                $user->save();

                $message = 'Your password has been reset.';
                Session::put('successMessage', $message);

                return redirect()->route('admin.login');
            }else{
                $message = 'please use the reset link to your mail';
                return $this->returnResponseWithErrorMessage($message);
            }



        }catch(\Exception $error){
            Log::info('error message: ' . $error->getMessage());

            $message = 'Unable to complete request, please try again';
            return $this->returnResponseWithErrorMessage($message);
        }
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function resetValidator(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|min:6|same:re-type_password',
            'token' =>'required'
        ]);
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|exists:users,email',
        ]);
    }

    private function tokenSentToUserMail($request){
        try{
            $user = User::where('email', $request->input('email'))->first();
            if($user){
                $user->token = Str::random(50);
                $user->save();

                Mail::to($request->input('email'))
                    ->send(new PasswordRecoveryMail($user));
            }

            return true;
        }catch(\Exception $error){
            Log::info('error message: ' . $error->getMessage());

            return false;
        }
    }

    private function returnResponseWithErrorMessage($message){

        Session::put('errorMessage', $message);

        return redirect()->back();

    }
}
