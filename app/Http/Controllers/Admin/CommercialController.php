<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Paths;
use Exception, Log, Validator, Session,  Storage, File;
use App\Models\AssetModel;
use App\Models\ImageModel;
use App\Models\LGAModel;
use App\Models\LocalityModel;
use App\Models\CommercialPropertyModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommercialController extends Controller
{
    public function __construct() {
		$this->middleware('admin');
    }

    public function index(){
        try{
            $commercialProperties = CommercialPropertyModel::all();
            $lgas = LGAModel::where('state_id', 33)->get();


            $data = [
                'page' => 'commercialProperties',
                'lgas' => $lgas,
                'commercialProperties'  => $commercialProperties,
            ];

            return view('Admin.Assets.commercial-properties', $data);
        }catch(Exception $error){
            Log::info('CommercialController@index error message: ' . $error->getMessage());
            $message = 'Unable to fetch Assets. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function addCommercialProperty(Request $request){
        try{
            $validator =  $this->validator($request->all());


            if($validator->fails()){
                $message = 'Please fill the form correctly';
                $errors = $validator->getMessageBag()->toArray();

                return $this->handleAjaxError($message, 400, $errors);
            }

            $asset = new AssetModel();
            $asset->title = $request->title;
            $asset->slug = str_replace(" ","-", $request->title);
            $asset->sale_type = $request->sale_type;
            $asset->category = $request->category;
            $asset->address = $request->address;
            $asset->description = $request->description;
            $asset->price = $request->price;
            $asset->status = $request->status;
            $asset->lga_id = $request->lga_id;
            $asset->locality_id = $request->locality_id;
            $asset->save();

            $commercialProperty = new CommercialPropertyModel();
            $commercialProperty->asset_id = $asset->id;
            $commercialProperty->type = $request->type;
            $commercialProperty->square_meters = $request->square_meters;
            $commercialProperty->parking_space = $request->parking_space;
            $commercialProperty->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Asset Added',
                'url'=> route('admin.assets.commercialProperties.edit', $commercialProperty->id)
            ]);

            return $request->all();
        }catch(Exception $error){
            Log::info('CommercialController@addCommercialProperty error message: ' . $error->getMessage());
            $message = 'Unable to store Assets. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    public function editCommercialProperty($CommercialPropertyId){
        try{
            $commercialProperty = CommercialPropertyModel::where('id', $CommercialPropertyId)->first();
            $lgas = LGAModel::where('state_id', 33)->get();
            if(!$commercialProperty){
                $message = 'Unable to get Asset. Commercial Property not found.';
                return $this->handleError($message);
            }
            $localities = LocalityModel::where('lga_id', $commercialProperty->asset->lga_id)->get();

            $data = [
                'page' => 'commercialProperties',
                'lgas' => $lgas,
                'localities' => $localities,
                'commercialProperty' => $commercialProperty,
            ];


            return view('Admin.Assets.edit-commercial-properties', $data);
        }catch(Exception $error){
            Log::info('CommercialController@editCommercialProperty error message: ' . $error->getMessage());
            $message = 'Unable to fetch Assets. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function updateCommercialProperty(Request $request){
        try{
            $requestAsset = json_decode($request->asset);
            $requestCommercialProperty = json_decode($request->commercialProperty);
            if(!$requestCommercialProperty || !$requestAsset){
                $message = 'Unable to edit Commercial Property. Fill the form correctly.';
                return $this->handleAjaxError($message, 400);
            }

            $asset = AssetModel::where('id', $requestAsset->id)->first();
            if(!$asset){
                $message = 'Unable to edit Commercial Property. Commercial Property not found.';
                return $this->handleAjaxError($message, 404);
            }

            $asset->title = $requestAsset->title;
            $asset->slug = str_replace(" ","-", $requestAsset->title);
            $asset->sale_type = $requestAsset->sale_type;
            $asset->category = $requestAsset->category;
            $asset->address = $requestAsset->address;
            $asset->description = $requestAsset->description;
            $asset->price = $requestAsset->price;
            $asset->status = $requestAsset->status;
            $asset->lga_id = $requestAsset->lga_id;
            $asset->locality_id = $requestAsset->locality_id;
            $asset->save();

            $commercialProperty = CommercialPropertyModel::where('id', $requestCommercialProperty->id)->first();

            if(!$commercialProperty){
                $message = 'Unable to edit Commercial Property. Commercial Property not found.';
                return $this->handleAjaxError($message, 404);
            }

            $commercialProperty->type = $requestCommercialProperty->type;
            $commercialProperty->square_meters = $requestCommercialProperty->square_meters;
            $commercialProperty->parking_space = $requestCommercialProperty->parking_space;
            $commercialProperty->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Asset Updated',
                'url'=> route('admin.assets.commercialProperties.edit', $commercialProperty->id)
            ]);

        }catch(Exception $error){
            Log::info('AssetController@updateCommercialProperty error message: ' . $error->getMessage());
            $message = 'Unable to store Asset. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    public function saveImages(Request $request){
        try{
            if($request->hasfile('files') && $request->input('property_id')){
                $propertyId = $request->input('property_id');
                $commercialProperty = CommercialPropertyModel::where('id', $propertyId)->first();
                if(!$commercialProperty){
                    $message = 'Unable to edit Commercial Property. Asset not found.';
                    return $this->handleAjaxError($message, 404);
                }

                foreach($request->file('files') as $image){
                    $imageName = $this->storeAssetImage($commercialProperty->asset, $image);
                    $image = new ImageModel;
                    $image->asset_id = $commercialProperty->asset->id;
                    $image->src = $imageName;
                    $image->save();
                }
                return response()->json([
                    'status' => 'success',
                    'message' => 'Asset Updated',
                    'images'=> $commercialProperty->images
                ]);
            }

            $message = 'Unable to edit Commercial Property. Fill the form correctly.';
            return $this->handleAjaxError($message, 400);


        }catch(Exception $error){
            Log::info('CommercialController@saveImages error message: ' . $error->getMessage());
            $message = 'Unable to store Images. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    private function storeAssetImage($asset, $image){
        $fileName = str_replace(' ', '_',$asset->title).str_replace(' ', '_',$image->getClientOriginalName()).'-'
            .time().'.'.$image->extension();
        $documentPath = Paths::ASSET_PATH.$fileName;
        Storage::put($documentPath, File::get($image));

        return $fileName;

    }

    public function deleteCommercialProperty($assetId){
        try{
            $asset = AssetModel::where('id', $assetId)->first();
            if(!$asset){
                $message = 'Unable to edit Commercial Property. Asset not found.';
                return $this->handleError($message);
            }

            $commercialProperty = CommercialPropertyModel::where('asset_id', $assetId)->first();
            if(!$commercialProperty){
                $message = 'Unable to edit Commercial Property. Commercial Property not found.';
                return $this->handleError($message);
            }

            $this->deleteAssetImages($assetId);
            $commercialProperty->delete();
            $asset->delete();

            $message = "Asset deleted successfully";
            Session::put('successMessage', $message);
            return redirect()->back();


        }catch(Exception $error){
            Log::info('CommercialController@deleteCommercialProperty error message: ' . $error->getMessage());
            $message = 'Unable to delete asset. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function deleteImage(Request $request){
        try{
            $commercialProperty = CommercialPropertyModel::where('id', $request->property_id)->first();
            if(!$commercialProperty){
                $message = 'Unable to delete Image. Commercial Property not found.';
                return $this->handleAjaxError($message, 404);
            }
            $image = ImageModel::where('id', $request->image_id)->where('asset_id', $commercialProperty->asset->id)->first();

            if(!$image){
                $message = 'Unable to delete Image. Image not found.';
                return $this->handleAjaxError($message, 404);
            }

            $this->deleteAssetImage($image);
            $image->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Image Deleted',
                'images'=> $commercialProperty->images
            ]);
        }catch(Exception $error){
            Log::info('AssetController@deleteImage error message: ' . $error->getMessage());
            $message = 'Unable to delete Images. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    private function deleteAssetImage($image){
        $imageName = basename($image->src);
        if(Storage::has(Paths::ASSET_PATH.$imageName)){
            Storage::delete(Paths::ASSET_PATH.$imageName);
        }
    }

    private function deleteAssetImages($assetId){
        $images = ImageModel::where('asset_id', $assetId)->get();
        foreach($images as $image){
            $this->deleteAssetImage($image);
            $image->delete();
        }
    }


    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string',
            'sale_type' => 'required|string',
            'category' => 'required|string',
            'address' => 'required|string',
            'description' => 'required|string',
            'price' => 'required',
            'status' => 'required|string',
            // 'square_meters' =>'required',
            // 'parking_space' =>'parking_space',
            'type' => 'required|string',
        ]);
    }

    private function handleAjaxError($message, $errorCode, $errors = []){
        return response()->json([
            'status' => 'error',
            'errors' => $errors,
            'message' => $message
        ], $errorCode);
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
