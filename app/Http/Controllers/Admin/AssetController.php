<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\AssetModel;
use App\Models\ApartmentModel;
use App\Http\Controllers\Controller;
use App\Models\LocalityModel;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    public function __construct() {
		$this->middleware('admin');
    }

    public function index(){
        try{
            $assets = [];

            $data = [
                'page' => 'assets',
                'assets' => $assets,
            ];


            return view('Admin.Assets.index', $data);
        }catch(Exception $error){
            Log::info('AssetController@index error message: ' . $error->getMessage());
            $message = 'Unable to fetch Assets. Encountered an error.';
            return $this->handleError($message, $error);
        }

    }

    public function fetchLocalities(Request $request){
        try{
            $localities = LocalityModel::where('lga_id', $request->id)->get();

            return response()->json([
                'status' => 'success',
                'localities'=> $localities
            ]);
        }catch(Exception $error){
            Log::info('AssetController@fetchLocalities error message: ' . $error->getMessage());
            $message = 'Unable to store Images. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    // public function apartments(){
    //     try{
    //         $apartments = [];

    //         $data = [
    //             'page' => 'apartments',
    //             'apartments' => $apartments,
    //         ];


    //         return view('Admin.Assets.apartments', $data);
    //     }catch(Exception $error){
    //         Log::info('AssetController@index error message: ' . $error->getMessage());
    //         $message = 'Unable to fetch Assets. Encountered an error.';
    //         return $this->handleError($message);
    //     }
    // }

    // public function addApartment(Request $request){
    //     try{
    //         $asset = new AssetModel();
    //         $asset->title = $request->title;
    //         $asset->sale_type = $request->sale_type;
    //         $asset->category = $request->category;
    //         $asset->address = $request->address;
    //         $asset->description = $request->description;
    //         $asset->price = $request->price;
    //         $asset->status = $request->status;
    //         $asset->save();
    //         return response()->json([
    //             'status' => 'success',
    //             'message' => 'Asset Added',
    //             'url'=> route('admin.assets.apartments.edit', $asset->id)
    //         ]);

    //         return $request->all();
    //     }catch(Exception $error){
    //         Log::info('AssetController@addApartment error message: ' . $error->getMessage());
    //         $message = 'Unable to fetch Assets. Encountered an error.';
    //         return $this->handleAjaxError($message, 500);
    //     }
    // }

    private function handleAjaxError($message, $errorCode){
        return response()->json([
            'status' => 'error',
            'message' => $message
        ], $errorCode);
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
