<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Paths;
use Exception, Log, Validator, Session,  Storage, File;
use App\Models\AssetModel;
use App\Models\ImageModel;
use App\Models\ApartmentModel;
use App\Http\Controllers\Controller;
use App\Models\LGAModel;
use App\Models\LocalityModel;
use Illuminate\Http\Request;

class ApartmentsController extends Controller
{
    public function __construct() {
		$this->middleware('admin');
    }

    public function apartments(){
        try{
            $apartments = ApartmentModel::all();
            $lgas = LGAModel::where('state_id', 33)->get();

            $data = [
                'page' => 'apartments',
                'lgas' => $lgas,
                'apartments' => $apartments,
            ];


            return view('Admin.Assets.apartments', $data);
        }catch(Exception $error){
            Log::info('AssetController@index error message: ' . $error->getMessage());
            $message = 'Unable to fetch Assets. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function addApartment(Request $request){
        try{
            $validator =  $this->validator($request->all());


            if($validator->fails()){
                $message = 'Please fill the form correctly';
                $errors = $validator->getMessageBag()->toArray();

                return $this->handleAjaxError($message, 400, $errors);
            }

            $asset = new AssetModel();
            $asset->title = $request->title;
            $asset->slug = str_replace(" ","-", $request->title);
            $asset->sale_type = $request->sale_type;
            $asset->category = $request->category;
            $asset->address = $request->address;
            $asset->description = $request->description;
            $asset->price = $request->price;
            $asset->status = $request->status;
            $asset->lga_id = $request->lga_id;
            $asset->locality_id = $request->locality_id;
            $asset->save();

            $apartment = new ApartmentModel();
            $apartment->asset_id = $asset->id;
            $apartment->type = $request->type;
            $apartment->total_rooms = $request->rooms;
            $apartment->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Asset Added',
                'url'=> route('admin.assets.apartments.edit', $apartment->id)
            ]);

            return $request->all();
        }catch(Exception $error){
            Log::info('AssetController@addApartment error message: ' . $error->getMessage());
            $message = 'Unable to store Assets. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    public function editApartment($apartmentId){
        try{
            $apartment = ApartmentModel::where('id', $apartmentId)->first();
            $lgas = LGAModel::where('state_id', 33)->get();
            if(!$apartment){
                $message = 'Unable to get Asset. Apartment not found.';
                return $this->handleError($message);
            }
            $localities = LocalityModel::where('lga_id', $apartment->asset->lga_id)->get();

            $data = [
                'page' => 'apartments',
                'lgas' => $lgas,
                'localities' => $localities,
                'apartment' => $apartment,
            ];

            return view('Admin.Assets.edit-apartment', $data);
        }catch(Exception $error){
            Log::info('AssetController@index error message: ' . $error->getMessage());
            $message = 'Unable to fetch Assets. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function updateApartment(Request $request){
        try{
            $requestAsset = json_decode($request->asset);
            $requestApartment = json_decode($request->apartment);
            if(!$requestApartment || !$requestAsset){
                $message = 'Unable to edit Apartment. Fill the form correctly.';
                return $this->handleAjaxError($message, 400);
            }

            $asset = AssetModel::where('id', $requestAsset->id)->first();
            if(!$asset){
                $message = 'Unable to edit Apartment. Asset not found.';
                return $this->handleAjaxError($message, 404);
            }

            $asset->title = $requestAsset->title;
            $asset->slug = str_replace(" ","-", $requestAsset->title);
            $asset->sale_type = $requestAsset->sale_type;
            $asset->category = $requestAsset->category;
            $asset->address = $requestAsset->address;
            $asset->description = $requestAsset->description;
            $asset->price = $requestAsset->price;
            $asset->status = $requestAsset->status;

            $asset->lga_id = $requestAsset->lga_id;
            $asset->locality_id = $requestAsset->locality_id;

            $asset->save();

            $apartment = ApartmentModel::where('id', $requestApartment->id)->first();

            if(!$apartment){
                $message = 'Unable to edit Apartment. Apartment not found.';
                return $this->handleAjaxError($message, 404);
            }

            $apartment->type = $requestApartment->type;
            $apartment->total_rooms = $requestApartment->total_rooms;
            $apartment->bedrooms = $requestApartment->bedrooms;
            $apartment->bathrooms = $requestApartment->bathrooms;
            $apartment->square_meters = $requestApartment->square_meters;
            $apartment->parking_space = $requestApartment->parking_space;
            $apartment->furnishing = $requestApartment->furnishing;
            $apartment->features = $requestApartment->features;
            $apartment->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Asset Updated',
                'url'=> route('admin.assets.apartments.edit', $apartment->id)
            ]);

        }catch(Exception $error){
            Log::info('AssetController@addApartment error message: ' . $error->getMessage());
            $message = 'Unable to store Assets. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    public function saveImages(Request $request){
        try{
            if($request->hasfile('files') && $request->input('apartment_id')){
                $apartmentId = $request->input('apartment_id');
                $apartment = ApartmentModel::where('id', $apartmentId)->first();
                if(!$apartment){
                    $message = 'Unable to edit Apartment. Apartment not found.';
                    return $this->handleAjaxError($message, 404);
                }

                foreach($request->file('files') as $image){
                    $imageName = $this->storeAssetImage($apartment->asset, $image);
                    $image = new ImageModel;
                    $image->asset_id = $apartment->asset->id;
                    $image->src = $imageName;
                    $image->save();
                }
                return response()->json([
                    'status' => 'success',
                    'message' => 'Asset Updated',
                    'images'=> $apartment->images
                ]);
            }

            $message = 'Unable to edit Apartment. Fill the form correctly.';
            return $this->handleAjaxError($message, 400);


        }catch(Exception $error){
            Log::info('AssetController@saveImages error message: ' . $error->getMessage());
            $message = 'Unable to store Images. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    public function deleteApartment($assetId){
        try{
            $asset = AssetModel::where('id', $assetId)->first();
            if(!$asset){
                $message = 'Unable to edit Apartment. Asset not found.';
                return $this->handleError($message);
            }

            $apartment = ApartmentModel::where('asset_id', $assetId)->first();
            if(!$apartment){
                $message = 'Unable to edit Apartment. Apartment not found.';
                return $this->handleError($message);
            }

            $this->deleteAssetImages($assetId);
            $apartment->delete();
            $asset->delete();

            $message = "Asset deleted successfully";
            Session::put('successMessage', $message);
            return redirect()->back();


        }catch(Exception $error){
            Log::info('AssetController@deleteApartment error message: ' . $error->getMessage());
            $message = 'Unable to delete asset. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function deleteImage(Request $request){
        try{
            $apartment = ApartmentModel::where('id', $request->apartment_id)->first();
            if(!$apartment){
                $message = 'Unable to delete Image. Apartment not found.';
                return $this->handleAjaxError($message, 404);
            }
            $image = ImageModel::where('id', $request->image_id)->where('asset_id', $apartment->asset->id)->first();

            if(!$image){
                $message = 'Unable to delete Image. Image not found.';
                return $this->handleAjaxError($message, 404);
            }

            $this->deleteAssetImage($image);
            $image->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Image Deleted',
                'images'=> $apartment->images
            ]);
        }catch(Exception $error){
            Log::info('AssetController@deleteImage error message: ' . $error->getMessage());
            $message = 'Unable to delete Images. Encountered an error.';
            return $this->handleAjaxError($message, 500);
        }
    }

    private function storeAssetImage($asset, $image){
        $fileName = str_replace(' ', '_',$asset->title).str_replace(' ', '_',$image->getClientOriginalName()).'-'
            .time().'.'.$image->extension();
        $documentPath = Paths::ASSET_PATH.$fileName;
        Storage::put($documentPath, File::get($image));

        return $fileName;

    }

    private function deleteAssetImage($image){
        $imageName = basename($image->src);
        if(Storage::has(Paths::ASSET_PATH.$imageName)){
            Storage::delete(Paths::ASSET_PATH.$imageName);
        }
    }

    private function deleteAssetImages($assetId){
        $images = ImageModel::where('asset_id', $assetId)->get();
        foreach($images as $image){
            $this->deleteAssetImage($image);
            $image->delete();
        }
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string',
            'sale_type' => 'required|string',
            'category' => 'required|string',
            'address' => 'required|string',
            'description' => 'required|string',
            'price' => 'required',
            'status' => 'required|string',
            'rooms' =>'required',
            'type' => 'required|string',
        ]);
    }

    private function handleAjaxError($message, $errorCode, $errors = []){
        return response()->json([
            'status' => 'error',
            'errors' => $errors,
            'message' => $message
        ], $errorCode);
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
