<?php

namespace App\Http\Controllers\Admin;

use Auth, Validator, Exception;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function __construct() {
		$this->middleware('admin');
    }

    public function index(){
        $data = [
            "page" => "settings"
        ];

        return view('Admin.settings', $data);
    }
    public function updateDetails(Request $request){
        try{
            $user = User::where('id', Auth::user()->id)->first();
            if(!$user){
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "user not found"
                ], 404);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users,email,'.$user->id,
            ]);
            if($validator->fails()){
                return response()->json([
                    'error' => true,
                    'status_code' => 400,
                    "message" => "Invalid data",
                    "errors" => $validator->messages()
                ], 400);
            }

            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();

            return response()->json([
                'error' => false,
                'message' => "Account was updated successfully"
            ], 200);

        }catch(Exception $error){
            Log::info('SettingsController@updateDetails error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function updatePassword(Request $request){
        try{
            $user = User::where('id', Auth::user()->id)->first();
            if(!$user){
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Admin not found"
                ], 404);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'old_password' => 'required',
                'password' => 'required|confirmed|min:6',

            ]);
            if($validator->fails()){
                return response()->json([
                    'error' => true,
                    'status_code' => 400,
                    "message" => "All password fields are required. Password must match password confirmation",
                    "errors" => $validator->messages()
                ], 400);
            }

            if (!\Hash::check($request->input('old_password'), Auth::user()->password)) {
                $message = "Old Password is incorrect";
                return response()->json([
                    'error' => true,
                    'status_code' => 400,
                    "message" => $message
                ], 400);
            }


            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json([
                'error' => false,
                'message' => "Account password was updated successfully"
            ], 200);

        }catch(Exception $error){
            Log::info('SettingsController@updatePassword error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }
}
